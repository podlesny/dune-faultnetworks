#ifndef DEBUG_UTILS_
#define DEBUG_UTILS_

#include <string>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/bitsetvector.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

using namespace std;

    template <typename ctype=double>
    void print(const Dune::BlockVector<Dune::FieldVector<ctype, 1>>& x, std::string message){
       std::cout << message << std::endl;
       for (size_t i=0; i<x.size(); i++) {
           std::cout << x[i] << " ";
       }
       std::cout << std::endl << std::endl;
   }

   template <typename ctype=double>
   void print(const Dune::BCRSMatrix<Dune::FieldMatrix<ctype, 1, 1>>& mat, std::string message){
       std::cout << message << std::endl;
       for (size_t i=0; i<mat.N(); i++) {
           for (size_t j=0; j<mat.M(); j++) {
               if (mat.exists(i,j))
                   std::cout << mat[i][j] << " ";
               else
                   std::cout << "0 ";
           }
           std::cout << std::endl;
       }
       std::cout << std::endl;
   }

   template <class T=Dune::FieldMatrix<double,1,1>>
   void print(const Dune::Matrix<T>& mat, std::string message){
       std::cout << message << std::endl;
       for (size_t i=0; i<mat.N(); i++) {
           for (size_t j=0; j<mat.M(); j++) {
                   std::cout << mat[i][j] << " ";
           }
           std::cout << std::endl;
       }
       std::cout << std::endl;
   }

   template <int dim, typename ctype=double>
   void print(const Dune::FieldVector<ctype, dim>& x, std::string message){
      std::cout << message << std::endl;
      for (size_t i=0; i<x.size(); i++) {
          std::cout << x[i] << " ";
      }
      std::cout << std::endl << std::endl;
   }


   void print(const Dune::BitSetVector<1>& x, std::string message){
      std::cout << message << std::endl;
      for (size_t i=0; i<x.size(); i++) {
          std::cout << x[i][0] << " ";
      }
      std::cout << std::endl << std::endl;
   }

   template <class T>
   void print(const std::vector<T>& x, std::string message){
      std::cout << message << std::endl;
      for (size_t i=0; i<x.size(); i++) {
          std::cout << x[i] << " ";
      }
      std::cout << std::endl << std::endl;
   }

   void print(const std::vector<Dune::FieldVector<double,1>>& x, std::string message){
      std::cout << message << std::endl;
      for (size_t i=0; i<x.size(); i++) {
          std::cout << x[i] << " ";
      }
      std::cout << std::endl << std::endl;
   }

   void print(const std::set<size_t>& x, std::string message){
      std::cout << message << std::endl;
      std::set<size_t>::iterator dofIt = x.begin();
      std::set<size_t>::iterator dofEndIt = x.end();
      for (; dofIt != dofEndIt; ++dofIt) {
          std::cout << *dofIt << " ";
      }
      std::cout << std::endl << std::endl;
   }

   void print(const std::set<int>& x, std::string message){
      std::cout << message << std::endl;
      std::set<int>::iterator dofIt = x.begin();
      std::set<int>::iterator dofEndIt = x.end();
      for (; dofIt != dofEndIt; ++dofIt) {
          std::cout << *dofIt << " ";
      }
      std::cout << std::endl << std::endl;
   }

   void step(const double stepNumber, std::string message=""){
      std::cout << message << " Step " << stepNumber << "!" << std::endl;
   }

   template <class VectorType, class DGBasisType>
   void writeToVTK(const DGBasisType& basis, const VectorType& x, const std::string path, const std::string name) {
       Dune::VTKWriter<typename DGBasisType::GridView> vtkwriter(basis.getGridView());
       VectorType toBePlotted(x);

       toBePlotted.resize(basis.getGridView().indexSet().size(DGBasisType::GridView::Grid::dimension));

       std::ofstream lStream( "garbage.txt" );
       std::streambuf* lBufferOld = std::cout.rdbuf();
       std::cout.rdbuf( lStream.rdbuf() );

       vtkwriter.addVertexData(toBePlotted,"data");
       vtkwriter.pwrite(name, path, path);

       std::cout.rdbuf( lBufferOld );
   }

   template <class BasisType, typename ctype=double>
   void printBasisDofLocation(const BasisType& basis) {
       typedef typename BasisType::GridView GridView;

       const int dim = GridView::dimension;

       std::map<int, int> indexTransformation;
       std::map<int, Dune::FieldVector<ctype, dim>> indexCoords;


       const GridView& gridView = basis.getGridView();
       const int gridN = std::pow(gridView.indexSet().size(dim), 1.0/dim)-1;

       typedef typename GridView::template Codim<0>::Iterator ElementIterator;
       ElementIterator it = gridView.template begin<0>();
       ElementIterator end = gridView.template end<0>();
       for (; it != end; ++it) {
           const typename BasisType::LocalFiniteElement& tFE = basis.getLocalFiniteElement(*it);
           const auto& geometry = (*it).geometry();

           for(int i=0; i<geometry.corners(); ++i) {
               const auto& vertex = geometry.corner(i);
               const auto& local = geometry.local(vertex);

               int vertexIndex = vertex[0]*gridN;
               for (int j=1; j<dim; ++j){
                   vertexIndex += vertex[j]*gridN*std::pow(gridN+1, j);
               }

               const int localBasisSize = tFE.localBasis().size();
               std::vector<Dune::FieldVector<double, 1>, std::allocator<Dune::FieldVector<double, 1> > > localBasisRep(localBasisSize);
               tFE.localBasis().evaluateFunction(local, localBasisRep);

               for(int k=0; k<localBasisSize; ++k) {
                   if (localBasisRep[k]==1) {
                       int dofIndex = basis.index((*it), k);

                       if (indexTransformation.count(dofIndex)==0) {
                           indexTransformation[dofIndex] = vertexIndex;
                           indexCoords[dofIndex] = vertex;
                       }

                       break;
                   }
               }
           }
       }

       std::cout << std::endl;
       std::cout << "Coarse level: Geometric vertex indices vs. associated basis dof indices " << indexTransformation.size() << std::endl;
       std::map<int,int>::iterator mapIt = indexTransformation.begin();
       std::map<int,int>::iterator mapEndIt = indexTransformation.end();
       for (; mapIt!=mapEndIt; ++mapIt) {
           std::cout << mapIt->second << " => " << mapIt->first << " Coords: ";

           const Dune::FieldVector<ctype, dim>& coords = indexCoords[mapIt->first];
           for (size_t i=0; i<coords.size(); i++) {
               std::cout << coords[i] << " ";
           }
           std::cout << std::endl;
       }
   }
#endif
