#ifndef CELL_PATCH_PRECONDITIONER_HH
#define CELL_PATCH_PRECONDITIONER_HH

#include <string>

#include <dune/faultnetworks/preconditioners/levelpatchpreconditioner.hh>
#include <dune/faultnetworks/patchfactories/cellpatchfactory.hh>

template <class BasisType, class LocalAssembler, class LocalInterfaceAssembler, class MatrixType, class VectorType>
class CellPatchPreconditioner : public LevelPatchPreconditioner<BasisType, LocalAssembler, LocalInterfaceAssembler, MatrixType, VectorType> {
private:
    using Base = LevelPatchPreconditioner<BasisType, LocalAssembler, LocalInterfaceAssembler, MatrixType, VectorType>;
    using GridView = typename Base::GridView;
    using Mode = typename Base::Mode;

public:
    CellPatchPreconditioner(const LevelInterfaceNetwork<GridView>& levelInterfaceNetwork,
                             const BasisType& patchLevelBasis,
                             const LocalAssembler& localAssembler,
                             const std::vector<std::shared_ptr<LocalInterfaceAssembler>>& localInterfaceAssemblers,
                             const Mode mode = Mode::additive) :
          Base(levelInterfaceNetwork, patchLevelBasis, localAssembler, localInterfaceAssemblers, mode) {}

    void build() {
        const int patchLevel = this->patchLevelBasis_.faultNetwork().level();
        CellPatchFactory<BasisType> patchFactory(this->patchLevelBasis_, this->basis_, patchLevel, this->level_);
        const auto& localToGlobal = patchFactory.getLocalToGlobal();
        const auto& boundaryDofs = patchFactory.getBoundaryDofs();

        VectorType rhs;
        rhs.resize(this->matrix_.N());
        rhs = 0;

        std::cout << "CellPatchPreconditioner::build() level: " << this->level_ << std::endl;

        size_t cellCount = localToGlobal.size();
        this->localProblems_.resize(cellCount);

        for (size_t i=0; i<cellCount; i++) {
            using LocalProblemType = typename std::remove_reference<decltype(*(this->localProblems_[i]))>::type;
            this->localProblems_[i] = std::make_shared<LocalProblemType>(this->matrix_, rhs, localToGlobal[i], boundaryDofs[i]);
        }
    }
};

#endif

