#ifndef CELL_PATCH_FACTORY_HH
#define CELL_PATCH_FACTORY_HH

#include <set>
#include <queue>

#include <dune/common/bitsetvector.hh>
#include <dune/common/fvector.hh>

#include <dune/fufem/referenceelementhelper.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/faultnetworks/hierarchicleveliterator.hh>

template <class BasisType>
class CellPatchFactory
{
protected:
        typedef typename BasisType::GridView GV;
        typedef typename BasisType::LocalFiniteElement LFE;
	
	typedef typename GV::Grid GridType;
	typedef typename GridType::ctype ctype;
	static const int dim = GridType::dimension;
	typedef typename GridType::template Codim<0>::Entity Element;

	typedef typename Dune::LevelMultipleCodimMultipleGeomTypeMapper<GridType,  Dune::MCMGElementLayout > ElementMapper;
	
    const BasisType& coarseBasis_;
    const BasisType& fineBasis_;

    const int coarseLevel_;
    const int fineLevel_;

    const GridType& grid;
    const GV& coarseGridView;
    const GV& fineGridView;

    std::vector<std::vector<int>> cellLocalToGlobal;
    std::vector<Dune::BitSetVector<1>> cellBoundaryDofs;
    std::vector<std::vector<Element>> cellElements;
	
    ElementMapper mapper;

public:
        //setup 
    CellPatchFactory(const BasisType& coarseBasis, const BasisType& fineBasis, const int coarseLevel, const int fineLevel) :
            coarseBasis_(coarseBasis),
            fineBasis_(fineBasis),
            coarseLevel_(coarseLevel),
            fineLevel_(fineLevel),
            grid(coarseBasis_.getGridView().grid()),
            coarseGridView(coarseBasis_.getGridView()),
            fineGridView(fineBasis_.getGridView()),
            mapper(grid, coarseLevel_) {

        const auto& interfaces = coarseBasis_.faultNetwork();

        cellElements.resize(0);
	  
        Dune::BitSetVector<1> visited(mapper.size());
        visited.unsetAll();

        std::queue<Element> cellSeeds;
        cellSeeds.push(*coarseGridView. template begin <0>());
        size_t cellCount = 0;

        while (!cellSeeds.empty()) {
            const auto cellSeed = cellSeeds.front();
            cellSeeds.pop();

            if (visited[mapper.index(cellSeed)][0] == true)
                continue;

            cellCount++;

            std::vector<Element> coarseElements;

            std::queue<Element> elementQueue;
            elementQueue.push(cellSeed);

            cellLocalToGlobal.resize(cellCount);
            cellBoundaryDofs.resize(cellCount);
            std::set<int> dofs;
            std::set<int> boundaryDofs;

            while (!elementQueue.empty()) {
                const auto elem = elementQueue.front();
                const auto elemID = mapper.index(elem);
                elementQueue.pop();

                if (visited[elemID][0] == true)
                    continue;

                coarseElements.push_back(elem);
                visited[elemID][0] = true;

                for (const auto& is : intersections(coarseGridView, elem)) {
                    if (is.neighbor()) {
                        const auto neighbor = is.outside();
                        const auto neighborId = mapper.index(neighbor);

                        if (!(visited[neighborId][0])) {
                            if (interfaces.isInterfaceIntersection(is)) {
                                cellSeeds.push(neighbor);
                            } else {
                                elementQueue.push(neighbor);
                            }
                        }
                    }
                }
            }

            cellElements.resize(cellCount);
            auto& elements = cellElements.back();

            // construct fine patch
            for (size_t i=0; i<coarseElements.size(); i++) {
                const Element& elem = coarseElements[i];
                addFinePatchElements(elem, dofs, boundaryDofs, elements);
            }

            auto& localToGlobal = cellLocalToGlobal.back();
            auto& localBoundaryDofs = cellBoundaryDofs.back();

            localToGlobal.resize(dofs.size());
            localBoundaryDofs.resize(dofs.size());
            localBoundaryDofs.unsetAll();

            size_t i=0;
            for (const auto& dof : dofs) {
                localToGlobal[i] = dof;

                if (boundaryDofs.count(dof)) {
                    localBoundaryDofs[i][0] = true;
                }
                i++;
            }
        }
	}

    auto& getLocalToGlobal() {
        return cellLocalToGlobal;
	}
	
    auto& getRegionElements() {
        return cellElements;
	}
	
    auto& getBoundaryDofs() {
        return cellBoundaryDofs;
	}

private:
    bool containsInsideSubentity(const Element& elem, const typename GridType::LevelIntersection& intersection, int subEntity, int codim) {
        return ReferenceElementHelper<double, dim>::subEntityContainsSubEntity(elem.type(), intersection.indexInInside(), 1, subEntity, codim);
    }

    void addFinePatchElements(const Element& coarseElem, std::set<int>& localDofs, std::set<int>& localBoundaryDofs, std::vector<Element>& fineElements) {
        using HierarchicLevelIteratorType = HierarchicLevelIterator<GridType>;
        HierarchicLevelIteratorType endHierIt(grid, coarseElem, HierarchicLevelIteratorType::PositionFlag::end, fineLevel_);
        for (HierarchicLevelIteratorType hierIt(grid, coarseElem, HierarchicLevelIteratorType::PositionFlag::begin, fineLevel_); hierIt!=endHierIt; ++hierIt) {

            const Element& fineElem = *hierIt;
            fineElements.push_back(fineElem);

            addLocalDofs(coarseElem, fineElem, localDofs, localBoundaryDofs);
        }
    }

    void addLocalDofs(const Element& coarseElem, const Element& fineElem, std::set<int>& localDofs, std::set<int>& localBoundaryDofs) {
        const LFE& fineLFE = fineBasis_.getLocalFiniteElement(fineElem);

        /*
        const auto& fineGeometry = fineElem.geometry();
        const auto& coarseGeometry = coarseElem.geometry();

        const auto& ref = Dune::ReferenceElements<ctype, dim>::general(fineElem.type()); */

        // insert dofs
        for (size_t i=0; i<fineLFE.localBasis().size(); ++i) {
            int dofIndex = fineBasis_.index(fineElem, i);
            localDofs.insert(dofIndex);
        }

        for (const auto& is : intersections(fineGridView, fineElem)) {

            // set boundaryDofs
            if (is.boundary()) {
                const auto& localCoefficients = fineLFE.localCoefficients();

                for (size_t i=0; i<localCoefficients.size(); i++) {
                    auto entity = localCoefficients.localKey(i).subEntity();
                    auto codim  = localCoefficients.localKey(i).codim();

                    if (containsInsideSubentity(fineElem, is, entity, codim))
                        localBoundaryDofs.insert(fineBasis_.index(fineElem, i));
                }
            }
        }
    }
};
#endif
