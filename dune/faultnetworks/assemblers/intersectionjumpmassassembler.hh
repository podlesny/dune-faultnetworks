#ifndef DUNE_FUFEM_INTERSECTION_JUMP_MASS_ASSEMBLER_HH
#define DUNE_FUFEM_INTERSECTION_JUMP_MASS_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/staticmatrixtools.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/assemblers/localassembler.hh>

#include <dune/faultnetworks/utils/debugutils.hh>

//** \brief Local assembler which assembles a mass matrix of the jumps over an intersection integral_[intersection]{jump(u)*jump(v)} **//
template <class GridView, class TrialLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1> >
class IntersectionJumpMassAssembler
{
  public:
    using Intersection = typename GridView::Intersection;
    typedef Dune::Matrix< Dune::FieldMatrix<int,1,1> > BoolMatrix;
    typedef typename Dune::Matrix<T> LocalMatrix;
    typedef typename T::field_type field_type;
    enum {ndofs = T::cols};

  private:
    static const int dim = GridView::dimension;
    LocalMatrix B_;
    const int quadOrder_;
    const double penaltyParam_;


  public:

    // standard jumps: B = [1 -1; -1 1];
    IntersectionJumpMassAssembler(LocalMatrix B, int quadOrder=2, double penaltyParam=1):
      B_(B),
      quadOrder_(quadOrder),
      penaltyParam_(penaltyParam)
  {
      // check if B_ 2x2 matrix
      if (B_.M()!=2 || B_.N()!=2) {
        DUNE_THROW(Dune::Exception, "B needs to be a 2x2 matrix!");
      }
  }

    //TODO Find out which local functions live on the intersection and not just take all
    // (This is kind of complicated because it depends on the dimension of the grid and on
    // the structure of the local basis.

    //if intersection belongs to boundary
    void indices(const Intersection& intersection,  BoolMatrix& isNonZero,
        const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {

      isNonZero = true;
      /*
         Dune::BitSetVector<1> livesOnIntersectaFE(aFE.localBasis().size(),false);
         Dune::BitSetVector<1> livesOnIntersecttFE(tFE.localBasis().size(),false);
      //if the finite element is P0/Q0 then it doesn't vanish on the intersection
      if (tFE.localBasis().size()==1)
      livesOnIntersectFE[0]=true;
      else
      const Dune::GenericReferenceElement<double,GridType::dim>& refElement = Dune::GenericReferenceElements<double, GridType::dim>::general(element->type());
      for (int i=0;i<tFE.localBasis().size();i++)
      {
      //check the local key if the i'th basis function lives on the intersection
      if (tFE.localCoefficients().localKey(i).codim()==1)
      {
      if (tFE.localCoefficients().localKey(i).subentity()==intersectSubIndex)
      livesOnIntersectFE[i]=true;
      }
      else if (tFE.localCoefficients().localKey(i).codim())


      }*/
    }

    //if intersection is interior
    void indices(const Intersection& intersection, BoolMatrix& isNonZero,
        const TrialLocalFE& tFEinside, const AnsatzLocalFE& aFEinside,
        const TrialLocalFE& tFEoutside, const AnsatzLocalFE& aFEoutside) const
    {

      isNonZero = true;

    }



    void assemble(const Intersection& intersection,
        LocalMatrix& localMatrix, const TrialLocalFE& tFE,
        const AnsatzLocalFE& aFE) const
    {
      typedef typename Dune::template FieldVector<double,dim> FVdim;
      //typedef typename Dune::template FieldMatrix<double,dim,dim> FMdimdim;
      typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType TrialRangeType;
      typedef typename AnsatzLocalFE::Traits::LocalBasisType::Traits::RangeType AnsatzRangeType;

      // Make sure we got suitable shape functions
      assert(tFE.type() == intersection.inside().type());
      assert(aFE.type() == intersection.inside().type());

      int rows = localMatrix.N();
      int cols = localMatrix.M();

      localMatrix = 0.0;

      // get quadrature rule
      const auto& quad = QuadratureRuleCache<double, dim-1>::rule(intersection.type(), quadOrder_,0);

      // store values of shape functions
      std::vector<TrialRangeType> trialValues(tFE.localBasis().size());
      std::vector<AnsatzRangeType> ansatzValues(aFE.localBasis().size());

      // loop over quadrature points
      for (size_t pt=0; pt < quad.size(); ++pt)
      {
        // Local position of the quadrature point
        const auto& quadPos = quad[pt].position();

        // get integration factor
        const double integrationElement = intersection.geometry().integrationElement(quadPos);

        // Position of the quadrature point within the element
        FVdim elementQuadPos = intersection.geometryInInside().global(quadPos);

        // evaluate basis functions
        tFE.localBasis().evaluateFunction(elementQuadPos, trialValues);
        aFE.localBasis().evaluateFunction(elementQuadPos, ansatzValues);

        // compute matrix entries
        double z = quad[pt].weight() * integrationElement;
        for(int i=0; i<rows; ++i)
        {
          double zi = trialValues[i]*z;

          for (int j=0; j<cols; ++j)
          {
            double zij = penaltyParam_ * B_[0][0] * ansatzValues[j] * zi;
            StaticMatrix::addToDiagonal(localMatrix[i][j],zij);

          }
        }
      }
      return;
    }

    void assemble(const Intersection& intersection, LocalMatrix& localMatrix,
        const TrialLocalFE& tFEinside, const AnsatzLocalFE& aFEinside,
        const TrialLocalFE& tFEoutside, const AnsatzLocalFE& aFEoutside
        ) const
    {
      typedef typename Dune::template FieldVector<double,dim> FVdim;
      //typedef typename Dune::template FieldMatrix<double,dim,dim> FMdimdim;
      //assume that RangeType is identical for the inside() and outside() element
      typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType TrialRangeType;
      typedef typename AnsatzLocalFE::Traits::LocalBasisType::Traits::RangeType AnsatzRangeType;

      // Make sure we got suitable shape functions
      assert(tFEinside.type() == intersection.inside().type());
      assert(aFEinside.type() == intersection.inside().type());
      assert(tFEoutside.type() == intersection.outside().type());
      assert(aFEoutside.type() == intersection.outside().type());

      localMatrix = 0.0;

      // get quadrature rule
      const auto& quad = QuadratureRuleCache<double, dim-1>::rule(intersection.type(), quadOrder_,0);

      // store values of shape functions
      std::vector<TrialRangeType> trialInsideValues(tFEinside.localBasis().size());
      std::vector<AnsatzRangeType> ansatzInsideValues(aFEinside.localBasis().size());
      std::vector<TrialRangeType> trialOutsideValues(tFEoutside.localBasis().size());
      std::vector<AnsatzRangeType> ansatzOutsideValues(aFEoutside.localBasis().size());
      // loop over quadrature points
      for (size_t pt=0; pt < quad.size(); ++pt)
      {
        // Local position of the quadrature point
        const Dune::FieldVector<double,dim-1>& quadPos = quad[pt].position();


        // get integration factor
        const double integrationElement = intersection.geometry().integrationElement(quadPos);

        // Position of the quadrature point within the elements
        FVdim insideQuadPos = intersection.geometryInInside().global(quadPos);
        FVdim outsideQuadPos = intersection.geometryInOutside().global(quadPos);

        // evaluate basis functions
        tFEinside.localBasis().evaluateFunction(insideQuadPos, trialInsideValues);
        aFEinside.localBasis().evaluateFunction(insideQuadPos, ansatzInsideValues);
        tFEoutside.localBasis().evaluateFunction(outsideQuadPos, trialOutsideValues);
        aFEoutside.localBasis().evaluateFunction(outsideQuadPos, ansatzOutsideValues);

        // compute matrix entries
        double z = quad[pt].weight() * integrationElement;
        for(size_t i=0; i<tFEinside.localBasis().size(); ++i)
        {
          double zi = trialInsideValues[i]*z;
          int counter =0;
          for (size_t j=0; j<aFEinside.localBasis().size(); ++j)
          {
            double zij = penaltyParam_ * B_[0][0] * ansatzInsideValues[j] * zi;
            StaticMatrix::addToDiagonal(localMatrix[i][counter++],zij);

          }

          for (size_t j=0; j<aFEoutside.localBasis().size(); ++j)
          {
            double zij = penaltyParam_ * B_[0][1] * ansatzOutsideValues[j] * zi;
            StaticMatrix::addToDiagonal(localMatrix[i][counter++],zij);

          }
        }

        int offset = tFEinside.localBasis().size();
        for(size_t i=0; i<tFEoutside.localBasis().size(); ++i)
        {
          double zi = trialOutsideValues[i]*z;
          int counter =0;
          for (size_t j=0; j<aFEinside.localBasis().size(); ++j)
          {
            double zij = penaltyParam_ * B_[1][0] * ansatzInsideValues[j] * zi;
            StaticMatrix::addToDiagonal(localMatrix[i+offset][counter++],zij);

          }

          for (size_t j=0; j<aFEoutside.localBasis().size(); ++j)
          {
            double zij = penaltyParam_ * B_[1][1] * ansatzOutsideValues[j] * zi;
            StaticMatrix::addToDiagonal(localMatrix[i+offset][counter++],zij);

          }
        }


      }
      return;
    }
};


#endif

