#ifndef FAULT_INTERFACE_HH
#define FAULT_INTERFACE_HH

#include <map>

#include <dune/common/bitsetvector.hh>
#include <dune/common/fvector.hh>

#include <dune/grid/common/mcmgmapper.hh>

template <typename GridView>
class FaultInterface : public std::enable_shared_from_this<FaultInterface<GridView>>
{
protected:
        static int idCounter;

        typedef typename GridView::Grid GridType;
        typedef typename GridType::LevelIntersection Intersection;
        typedef typename GridType::template Codim<0>::Entity Element;
        typedef typename Intersection::Geometry::GlobalCoordinate GlobalCoordinate;
        static const int dimElement = Element::dimension;

        typedef typename GridType::LevelIntersectionIterator LevelIntersectionIterator;

        typedef typename Dune::LevelMultipleCodimMultipleGeomTypeMapper <GridType,  Dune::MCMGElementLayout > ElementMapper;

        const GridView& gridView_;
        const GridType& grid_;
        const int level_;
        const int id_;
        const std::shared_ptr<FaultInterface> father_;

        ElementMapper elementMapper_;

        std::vector<Intersection> faces_;
        std::map<std::pair<size_t, size_t>, bool> isInterfaceIntersectionMapper_;

        std::set<size_t> interfaceDofs_;

        void computeIntersectionDofs(const Intersection& intersection, std::set<size_t>& intersectionDofs) {
			intersectionDofs.clear();
			
			// loop over all vertices of the intersection
			const Element& insideElement = intersection.inside();
			
			const auto& refElement = Dune::ReferenceElements<double,dimElement>::general(insideElement.type());
            for (int i=0; i<refElement.size(intersection.indexInInside(), 1, dimElement); i++) {
				size_t idxInElement = refElement.subEntity(intersection.indexInInside(), 1, i, dimElement);
                size_t globalIdx = gridView_.indexSet().subIndex(insideElement, idxInElement, dimElement);
                intersectionDofs.insert(globalIdx);
            }
        }
		
public:
        FaultInterface(const GridView& gridView, const int level, std::shared_ptr<FaultInterface> father = nullptr) :
            gridView_(gridView), grid_(gridView_.grid()), level_(level), id_(idCounter++), father_(father), elementMapper_(grid_, level_)
        {}

        bool isInterfaceIntersection(size_t elementIdx, size_t intersectionIdx) {
            return isInterfaceIntersectionMapper_.count(std::make_pair(elementIdx, intersectionIdx));
        }

        size_t getElementIndex(const Element& elem) {
            return elementMapper_.index(elem);
        }

        void addFace(const Intersection& face) {
			faces_.push_back(face);
			
			// add to interfaceDofs_
			std::set<size_t> faceDofs;
            computeIntersectionDofs(face, faceDofs);

			interfaceDofs_.insert(faceDofs.begin(), faceDofs.end());

			// add to isInterfaceIntersectionMapper_
            size_t insideElementIdx = elementMapper_.index(face.inside());
            isInterfaceIntersectionMapper_[std::make_pair(insideElementIdx, face.indexInInside())] = true;

            if (face.neighbor()) {
                size_t outsideElementIdx = elementMapper_.index(face.outside());
                isInterfaceIntersectionMapper_[std::make_pair(outsideElementIdx, face.indexInOutside())] = true;
            }
        }

        size_t dofCount() const {
			return interfaceDofs_.size();
        }
        
        // local index of interface dof
        size_t localIndex(const size_t i) const {
			std::set<size_t>::iterator it;
			
			it = interfaceDofs_.find(i);

			if (it == interfaceDofs_.end())
				DUNE_THROW(Dune::Exception, "Global index of dof not part of the interface.");

			return std::distance(interfaceDofs_.begin(), it);
		}
		
        std::shared_ptr<const FaultInterface> coarsestFather() const {
            std::shared_ptr<const FaultInterface> res = this->shared_from_this();

            while (res->father() != nullptr)
                res = res->father();

            return res;
        }

        const std::set<size_t>& getInterfaceDofs() const {
            return interfaceDofs_;
		}

        const ElementMapper& getElementMapper() const {
            return elementMapper_;
        }

        const std::vector<Intersection>& faces() const {
            return faces_;
        }

        int level() const {
            return level_;
        }

        int id() const {
            return id_;
        }

        std::shared_ptr<FaultInterface> father() const {
            return father_;
        }

        const GridType& grid() const {
            return grid_;
        }
};
template <typename GridView> int FaultInterface<GridView>::idCounter = 0;
#endif
