// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef INTERFACE_OPERATOR_ASSEMBLER_HH
#define INTERFACE_OPERATOR_ASSEMBLER_HH

#include <dune/istl/matrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/grid/common/mcmgmapper.hh>

#include "dune/fufem/staticmatrixtools.hh"


#include <dune/faultnetworks/faultinterface.hh>

// TODO Modify such that the global assembler also works for nonconforming grids!

//! Global Assembler for operators that are defined on the intersections of the grid
template <class TrialBasis, class AnsatzBasis>
class InterfaceOperatorAssembler
{
        //! Parameter for mapper class
        template<int dim>
        struct FaceMapperLayout
        {
            bool contains (Dune::GeometryType gt)
            {
                return gt.dim() == dim-1;
            }
        };


    private:
        typedef typename TrialBasis::GridView GridView;
        typedef typename GridView::Grid GridType;
        typedef typename GridType::LevelIntersection Intersection;
        typedef typename GridType::template Codim<0>::Entity Element;

    public:
        //! create assembler for grid
        InterfaceOperatorAssembler(const TrialBasis& tBasis, const AnsatzBasis& aBasis, const std::vector<Intersection>& intersections) :
            tBasis_(tBasis),
            aBasis_(aBasis),
            intersections_(intersections)
        {}


        template <class LocalAssemblerType>
        void addIndices(const std::vector<std::shared_ptr<LocalAssemblerType>>& localAssemblers, Dune::MatrixIndexSet& indices, const bool lumping=false) const
        {
            if (lumping)
                addIndicesStaticLumping<LocalAssemblerType,true>(localAssemblers, indices);
            else
                addIndicesStaticLumping<LocalAssemblerType,false>(localAssemblers, indices);
        }


        template <class LocalAssemblerType, class GlobalMatrixType>
        void addEntries(const std::vector<std::shared_ptr<LocalAssemblerType>>& localAssemblers, GlobalMatrixType& A, const bool lumping=false) const
        {
            if (lumping)
                addEntriesStaticLumping<LocalAssemblerType,GlobalMatrixType,true>(localAssemblers, A);
            else
                addEntriesStaticLumping<LocalAssemblerType,GlobalMatrixType,false>(localAssemblers, A);
        }


        template <class LocalAssemblerType, class GlobalMatrixType>
        void assemble(const std::vector<std::shared_ptr<LocalAssemblerType>>& localAssemblers, GlobalMatrixType& A, const bool lumping=false) const
        {
            int rows = tBasis_.size();
            int cols = aBasis_.size();

            Dune::MatrixIndexSet indices(rows, cols);

            addIndices(localAssemblers, indices, lumping);

            indices.exportIdx(A);
            A=0.0;

            addEntries(localAssemblers, A, lumping);

            return;
        }


    protected:

        template <class LocalAssemblerType, bool lumping>
        void addIndicesStaticLumping(const std::vector<std::shared_ptr<LocalAssemblerType>>& localAssemblers, Dune::MatrixIndexSet& indices) const
        {
            typedef typename LocalAssemblerType::BoolMatrix BoolMatrix;
            typedef typename TrialBasis::LinearCombination LinearCombination;

            assert(intersections_.size()==localAssemblers.size());

            for (size_t i=0; i<intersections_.size(); ++i) {
                    const Intersection& is = intersections_[i];
                    const LocalAssemblerType& localAssembler = *(localAssemblers[i]);
                    const Element& e = is.inside();

                    const auto& tFEinside = tBasis_.getLocalFiniteElement(e);
                    const auto& aFEinside = aBasis_.getLocalFiniteElement(e);


                    //if the face lies on the boundary
                    if (is.boundary())
                    {

                        BoolMatrix localIndices(tFEinside.localBasis().size(), aFEinside.localBasis().size());

                        localAssembler.indices(is, localIndices, tFEinside, aFEinside);

                        for (size_t i=0; i<tFEinside.localBasis().size(); ++i)
                        {
                            int rowIndex= tBasis_.index(e, i);
                            const LinearCombination& rowConstraints = tBasis_.constraints(rowIndex);
                            bool rowIsConstrained = tBasis_.isConstrained(rowIndex);

                            for (size_t j=0; j<aFEinside.localBasis().size(); ++j)
                            {
                                if (localIndices[i][j])
                                {
                                    if (lumping)
                                    {
                                        if (rowIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                                indices.add(rowConstraints[rw].index, rowConstraints[rw].index);
                                        }
                                        else
                                            indices.add(rowIndex, rowIndex);
                                    }
                                    else
                                    {
                                        int colIndex= aBasis_.index(e, j);
                                        const LinearCombination& colConstraints = aBasis_.constraints(colIndex);
                                        bool colIsConstrained = aBasis_.isConstrained(colIndex);
                                        if (rowIsConstrained and colIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                            {
                                                for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                                    indices.add(rowConstraints[rw].index, colConstraints[cw].index);
                                            }
                                        }
                                        else if (rowIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                                indices.add(rowConstraints[rw].index, colIndex);
                                        }
                                        else if (colIsConstrained)
                                        {
                                            for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                                indices.add(rowIndex, colConstraints[cw].index);
                                        }
                                        else
                                            indices.add(rowIndex, colIndex);
                                    }
                                }
                            }

                        }
                    }
                    else
                    {
                        //local basis of the outside() element
                        const auto& outside = is.outside();
                        const auto& tFEoutside = tBasis_.getLocalFiniteElement(outside);
                        const auto& aFEoutside = aBasis_.getLocalFiniteElement(outside);

                        BoolMatrix localIndices(tFEinside.localBasis().size()+tFEoutside.localBasis().size(), aFEinside.localBasis().size()+aFEoutside.localBasis().size());

                        localAssembler.indices(is, localIndices, tFEinside, aFEinside,  tFEoutside, aFEoutside);

                        int rowIndex = 0;
                        int colIndex = 0;
                        for (size_t i=0; i<tFEinside.localBasis().size()+tFEoutside.localBasis().size(); ++i)
                        {
                            if (i<tFEinside.localBasis().size())
                                rowIndex= tBasis_.index(e, i);
                            else
                                rowIndex = tBasis_.index(outside,i-tFEinside.localBasis().size());
                            const LinearCombination& rowConstraints = tBasis_.constraints(rowIndex);
                            bool rowIsConstrained = tBasis_.isConstrained(rowIndex);
                            for (size_t j=0; j<aFEinside.localBasis().size()+aFEoutside.localBasis().size(); ++j)
                            {
                                if (localIndices[i][j])
                                {
                                    if (lumping)
                                    {
                                        if (rowIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                                indices.add(rowConstraints[rw].index, rowConstraints[rw].index);
                                        }
                                        else
                                            indices.add(rowIndex, rowIndex);
                                    }
                                    else
                                    {
                                        if (j<tFEinside.localBasis().size())
                                            colIndex= aBasis_.index(e, j);
                                        else
                                            colIndex = aBasis_.index(outside,j-aFEinside.localBasis().size());
                                        const LinearCombination& colConstraints = aBasis_.constraints(colIndex);
                                        bool colIsConstrained = aBasis_.isConstrained(colIndex);
                                   if (rowIsConstrained and colIsConstrained)
                                   {
                                       for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                       {
                                           for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                               indices.add(rowConstraints[rw].index, colConstraints[cw].index);
                                       }
                                   }
                                   else if (rowIsConstrained)
                                   {
                                       for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                           indices.add(rowConstraints[rw].index, colIndex);
                                   }
                                   else if (colIsConstrained)
                                   {
                                       for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                           indices.add(rowIndex, colConstraints[cw].index);
                                   }
                                   else
                                       indices.add(rowIndex, colIndex);
                                    }
                                }
                            }
                        }
                    }
                }

        }

        template <class LocalAssemblerType, class GlobalMatrixType, bool lumping>
        void addEntriesStaticLumping(const std::vector<std::shared_ptr<LocalAssemblerType>>& localAssemblers, GlobalMatrixType& A) const
        {
            typedef typename LocalAssemblerType::LocalMatrix LocalMatrix;
            typedef typename TrialBasis::LinearCombination LinearCombination;

            assert(intersections_.size()==localAssemblers.size());

            for (size_t i=0; i<intersections_.size(); ++i) {
                    const Intersection& is = intersections_[i];
                    const LocalAssemblerType& localAssembler = *(localAssemblers[i]);
                    const Element& e = is.inside();

                    const auto& tFEinside = tBasis_.getLocalFiniteElement(e);
                    const auto& aFEinside = aBasis_.getLocalFiniteElement(e);

                    //if the face lies on the boundary
                    if (is.boundary())
                    {

                        LocalMatrix localA(tFEinside.localBasis().size(), aFEinside.localBasis().size());
                        localAssembler.assemble(is, localA, tFEinside, aFEinside);

                        for (size_t i=0; i<tFEinside.localBasis().size(); ++i)
                        {
                            int rowIndex = tBasis_.index(e, i);
                            const LinearCombination& rowConstraints = tBasis_.constraints(rowIndex);
                            bool rowIsConstrained = tBasis_.isConstrained(rowIndex);
                            for (size_t j=0; j<aFEinside.localBasis().size(); ++j)
                            {

                                if (localA[i][j].infinity_norm()!=0.0)
                                {
                                    if (lumping)
                                    {
                                        if (rowIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                                Arithmetic::addProduct(A[rowConstraints[rw].index][rowConstraints[rw].index], rowConstraints[rw].factor, localA[i][j]);
                                        }
                                        else
                                            Arithmetic::addProduct(A[rowIndex][rowIndex], 1.0, localA[i][j]);
                                    }
                                    else
                                    {
                                        int colIndex = aBasis_.index(e, j);
                                        const LinearCombination& colConstraints = aBasis_.constraints(colIndex);
                                        bool colIsConstrained = aBasis_.isConstrained(colIndex);
                                        if (rowIsConstrained and colIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                            {
                                                for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                                    Arithmetic::addProduct(A[rowConstraints[rw].index][colConstraints[cw].index], rowConstraints[rw].factor * colConstraints[cw].factor, localA[i][j]);
                                            }
                                        }
                                        else if (rowIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                                Arithmetic::addProduct(A[rowConstraints[rw].index][colIndex], rowConstraints[rw].factor, localA[i][j]);
                                        }
                                        else if (colIsConstrained)
                                        {
                                            for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                                Arithmetic::addProduct(A[rowIndex][colConstraints[cw].index], colConstraints[cw].factor, localA[i][j]);
                                        }
                                        else
                                            Arithmetic::addProduct(A[rowIndex][colIndex], 1.0, localA[i][j]);
                                    }
                                }
                            }
                        }

                    }//if face is not part of the boundary
                    else
                    {

                        //local basis of the outside() element
                        const auto& outside = is.outside();
                        const auto& tFEoutside = tBasis_.getLocalFiniteElement(outside);
                        const auto& aFEoutside = aBasis_.getLocalFiniteElement(outside);

                        LocalMatrix localA(tFEinside.localBasis().size()+tFEoutside.localBasis().size(), aFEinside.localBasis().size()+aFEoutside.localBasis().size());
                        localAssembler.assemble(is, localA, tFEinside, aFEinside,  tFEoutside, aFEoutside);

                        int rowIndex = 0;
                        int colIndex = 0;

                        //when using this global face assembler with continous finite element spaces some
                        //local basis functions are part of the same global basis function.
                        //thus the we have to neglect the entries of the local matrix that
                        //belong to the same pairing of global basis functions we already added
                        std::set<std::pair<int,int> > sameGlobalPair;

                        for (size_t i=0; i<tFEinside.localBasis().size()+tFEoutside.localBasis().size(); ++i)
                        {
                            if (i<tFEinside.localBasis().size())
                                rowIndex= tBasis_.index(e, i);
                            else
                                rowIndex = tBasis_.index(outside,i-tFEinside.localBasis().size());
                            const LinearCombination& rowConstraints = tBasis_.constraints(rowIndex);
                            bool rowIsConstrained = tBasis_.isConstrained(rowIndex);
                            for (size_t j=0; j<aFEinside.localBasis().size()+aFEoutside.localBasis().size(); ++j)
                            {
                                if (localA[i][j].infinity_norm()==0.0)
                                    continue;

                                if (j<aFEinside.localBasis().size())
                                    colIndex= aBasis_.index(e, j);
                                else
                                    colIndex = aBasis_.index(outside,j-aFEinside.localBasis().size());

                                //if (i,j) has the same global index pair as one that has already been added
                                //to the global matrix then dismiss it
                                if (sameGlobalPair.find(std::pair<int,int>(rowIndex,colIndex)) != sameGlobalPair.end())
                                    continue;
                                else
                                    sameGlobalPair.insert(std::pair<int,int>(rowIndex,colIndex));

                                if (lumping)
                                {
                                    if (rowIsConstrained)
                                    {
                                        for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                            Arithmetic::addProduct(A[rowConstraints[rw].index][rowConstraints[rw].index], rowConstraints[rw].factor, localA[i][j]);
                                    }
                                    else
                                        Arithmetic::addProduct(A[rowIndex][rowIndex], 1.0, localA[i][j]);
                                }
                                else
                                {

                                    const LinearCombination& colConstraints = aBasis_.constraints(colIndex);
                                    bool colIsConstrained = aBasis_.isConstrained(colIndex);
                                    if (rowIsConstrained and colIsConstrained)
                                    {
                                        for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                        {
                                            for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                                Arithmetic::addProduct(A[rowConstraints[rw].index][colConstraints[cw].index], rowConstraints[rw].factor * colConstraints[cw].factor, localA[i][j]);
                                        }
                                    }
                                    else if (rowIsConstrained)
                                    {
                                        for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                            Arithmetic::addProduct(A[rowConstraints[rw].index][colIndex], rowConstraints[rw].factor, localA[i][j]);
                                    }
                                    else if (colIsConstrained)
                                    {
                                        for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                            Arithmetic::addProduct(A[rowIndex][colConstraints[cw].index], colConstraints[cw].factor, localA[i][j]);
                                    }
                                    else
                                        Arithmetic::addProduct(A[rowIndex][colIndex], 1.0, localA[i][j]);
                                }
                            }
                        }
                    }
                }
        }

        const TrialBasis& tBasis_;
        const AnsatzBasis& aBasis_;

        const std::vector<Intersection>& intersections_;
};

#endif

