#ifndef DG_MG_TRANSFER_HH
#define DG_MG_TRANSFER_HH

#include <dune/fufem/assemblers/basisinterpolationmatrixassembler.hh>

#include <dune/faultnetworks/faultp1nodalbasis.hh>
#include <dune/faultnetworks/utils/debugutils.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/bitsetvector.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/localfunctions/lagrange/pqkfactory.hh>

#include "dune/solvers/common/staticmatrixtools.hh"
#include <dune/solvers/common/arithmetic.hh>
#include <dune/solvers/transferoperators/genericmultigridtransfer.hh>

template <class DGBasis, class RT=double>
class DGMGTransfer {

private:
    typedef typename DGBasis::GridView GV;

    const DGBasis& coarseBasis_;
    const DGBasis& fineBasis_;

    const GV& coarseGridView_;
    const GV& fineGridView_;

    typedef typename Dune::BCRSMatrix<Dune::FieldMatrix<RT, 1, 1>> MatrixType;

    MatrixType prolongationMatrix_;

public:
    DGMGTransfer(const DGBasis& coarseBasis, const DGBasis& fineBasis):
        coarseBasis_(coarseBasis),
        fineBasis_(fineBasis),
        coarseGridView_(coarseBasis.getGridView()),
        fineGridView_(fineBasis.getGridView())
    {
        typedef typename MatrixType::block_type TransferMatrixBlock;
        typedef typename MatrixType::field_type field_type;
        typedef typename GV::ctype ctype;
        typedef typename GV::Grid GridType;

        const int dim = GV::dimension;

        int rows = fineBasis_.size();
        int cols = coarseBasis_.size();

        // A factory for the shape functions
        typedef typename DGBasis::FiniteElementCache FECache;
        typedef typename FECache::FiniteElementType FEType;
        FECache finiteElementCache;

        typedef typename GV::template Codim<0>::Iterator ElementIterator;

        ElementIterator fIt    = fineGridView_.template begin<0>();
        ElementIterator fEndIt = fineGridView_.template end<0>();


        // ///////////////////////////////////////////
        // Determine the indices present in the matrix
        // /////////////////////////////////////////////////
        Dune::MatrixIndexSet indices(rows, cols);

        for (; fIt != fEndIt; ++fIt) {

            const Dune::ReferenceElement<ctype,dim>& fineRefElement
                = Dune::ReferenceElements<ctype, dim>::general(fIt->type());

            // Get local finite element
            const FEType& fineBaseSet = finiteElementCache.get(fIt->type());

            const size_t numFineBaseFct = fineBaseSet.localBasis().size();

            std::vector<Dune::FieldVector<ctype,dim> > local(numFineBaseFct);

            for (size_t i=0; i<numFineBaseFct; i++)
            {
                const Dune::LocalKey& iLocalKey = fineBaseSet.localCoefficients().localKey(i);
                local[i] = fineRefElement.position(iLocalKey.subEntity(), iLocalKey.codim());
            }

            // Get ancestor element in the coarse grid view, and the local position there.
            typename GV::template Codim<0>::Entity ancestor(*fIt);

            while (not coarseGridView_.contains(ancestor) && ancestor.level() != 0 ) {

                typename GridType::template Codim<0>::LocalGeometry geometryInFather = ancestor.geometryInFather();

                for (size_t i=0; i<numFineBaseFct; i++)
                    local[i] = geometryInFather.global(local[i]);
                ancestor = ancestor.father();

            }

            assert(coarseGridView_.contains(ancestor));

            for (size_t j=0; j<numFineBaseFct; j++)
            {
                // Get local finite element
                const FEType& coarseBaseSet = finiteElementCache.get(ancestor.type());

                const size_t numCoarseBaseFct = coarseBaseSet.localBasis().size();

                // preallocate vector for function evaluations
                std::vector<Dune::FieldVector<field_type,1> > values(numCoarseBaseFct);

                // Evaluate coarse grid base functions
                coarseBaseSet.localBasis().evaluateFunction(local[j], values);

                const Dune::LocalKey& jLocalKey = fineBaseSet.localCoefficients().localKey(j);
                int globalFine = fineBasis_.index(*fIt, jLocalKey.subEntity());

                for (size_t i=0; i<numCoarseBaseFct; i++)
                {
                    if (values[i] > 0.001)
                    {
                        const Dune::LocalKey& iLocalKey = coarseBaseSet.localCoefficients().localKey(i);
                        int globalCoarse = coarseBasis_.index(ancestor, iLocalKey.subEntity());
                        indices.add(globalFine, globalCoarse);
                    }
                }
            }

        }

        indices.exportIdx(prolongationMatrix_);

        // /////////////////////////////////////////////
        // Compute the matrix
        // /////////////////////////////////////////////
        for (fIt = fineGridView_.template begin<0>(); fIt != fEndIt; ++fIt) {


            const Dune::ReferenceElement<ctype,dim>& fineRefElement
                = Dune::ReferenceElements<ctype, dim>::general(fIt->type());

            // Get local finite element
            const FEType& fineBaseSet = finiteElementCache.get(fIt->type());;

            const size_t numFineBaseFct = fineBaseSet.localBasis().size();

            std::vector<Dune::FieldVector<ctype,dim> > local(numFineBaseFct);

            for (size_t i=0; i<numFineBaseFct; i++)
            {
                const Dune::LocalKey& iLocalKey = fineBaseSet.localCoefficients().localKey(i);
                local[i] = fineRefElement.position(iLocalKey.subEntity(), iLocalKey.codim());
            }

            // Get ancestor element in the coarse grid view, and the local position there.
            typename GV::template Codim<0>::Entity ancestor(*fIt);

            while (not coarseGridView_.contains(ancestor) && ancestor.level() != 0 ) {

                typename GridType::template Codim<0>::LocalGeometry geometryInFather = ancestor.geometryInFather();

                for (size_t i=0; i<numFineBaseFct; i++)
                    local[i] = geometryInFather.global(local[i]);
                ancestor = ancestor.father();

            }

            assert(coarseGridView_.contains(ancestor));

            for (size_t j=0; j<numFineBaseFct; j++)
            {
                // Get local finite element
                const FEType& coarseBaseSet = finiteElementCache.get(ancestor.type());

                const size_t numCoarseBaseFct = coarseBaseSet.localBasis().size();

                // preallocate vector for function evaluations
                std::vector<Dune::FieldVector<field_type,1> > values(numCoarseBaseFct);

                // Evaluate coarse grid base functions
                coarseBaseSet.localBasis().evaluateFunction(local[j], values);

                const Dune::LocalKey& jLocalKey = fineBaseSet.localCoefficients().localKey(j);
                int globalFine = fineBasis_.index(*fIt, jLocalKey.subEntity());

                for (size_t i=0; i<numCoarseBaseFct; i++)
                {
                    if (values[i] > 0.001)
                    {
                        const Dune::LocalKey& iLocalKey = coarseBaseSet.localCoefficients().localKey(i);
                        int globalCoarse = coarseBasis_.index(ancestor, iLocalKey.subEntity());

                        prolongationMatrix_[globalFine][globalCoarse] = Dune::ScaledIdentityMatrix<ctype,TransferMatrixBlock::rows>(values[i]);
                    }
                }
            }

        }
    }




    /** \brief Prolong a function from the coarse onto the fine grid
     */
    template <class VectorType>
    void prolong(const VectorType& f, VectorType &t) const
    {
        GenericMultigridTransfer::prolong(prolongationMatrix_, f, t, -1);
    }

    /** \brief Restrict a function from the fine onto the coarse grid
     */
    template <class VectorType>
    void restrict(const VectorType& f, VectorType &t) const
    {
        GenericMultigridTransfer::restrict(prolongationMatrix_, f, t, -1);
    }

    template< class OperatorType>
    void galerkinRestrictSetOccupation(const OperatorType& fineMat, OperatorType& coarseMat)
    {
        GenericMultigridTransfer::galerkinRestrictSetOccupation(prolongationMatrix_, fineMat, coarseMat);
    }

    template<class FineMatrixType, class CoarseMatrixType>
    void galerkinRestrict(const FineMatrixType& fineMat, CoarseMatrixType& coarseMat)
    {
         GenericMultigridTransfer::galerkinRestrict(prolongationMatrix_, fineMat, coarseMat);
    }

    template <class BitVectorType>
    void restrictToFathers(const BitVectorType& f, BitVectorType& t) const
    {
        GenericMultigridTransfer::restrictBitFieldToFathers(prolongationMatrix_, f, t, -1);
    }
};

#endif
