#ifndef OSC_DATA_HIERARCHY_HH
#define OSC_DATA_HIERARCHY_HH

#include <memory>
#include <dune/faultnetworks/oscdata.hh>

template <class GridType, class MatrixType>
class OscDataHierarchy {
private:
    typedef std::shared_ptr<OscData<GridType, MatrixType>>& reference;
    typedef const std::shared_ptr<OscData<GridType, MatrixType>>& const_reference;

    typedef std::shared_ptr<OscData<GridType, MatrixType>>* iterator;
    typedef const std::shared_ptr<OscData<GridType, MatrixType>>* const_iterator;

    const GridType& grid_;
    
    std::vector<std::shared_ptr<OscData<GridType, MatrixType>>> oscDataHierarchy_;

public:
    OscDataHierarchy(const OscData<GridType, MatrixType>& oscData) : grid_(oscData.grid()) {
        assert(oscData.isSet());

        oscDataHierarchy_.resize(grid_.maxLevel()+1);

        for (size_t i=0; i<oscDataHierarchy_.size(); i++) {
            oscDataHierarchy_[i] = std::make_shared<OscData<GridType, MatrixType>>(grid_, i);
            oscData.adapt(*(oscDataHierarchy_[i]));
        }
    }

    reference operator[] (size_t i) {
        return oscDataHierarchy_[i];
    }
 
    const_reference operator[] (size_t i) const {
        return oscDataHierarchy_[i];
    }

    iterator begin() {
        return oscDataHierarchy_.begin();
    }

    const_iterator begin() const {
        return oscDataHierarchy_.begin();
    }

    iterator end() {
        return oscDataHierarchy_.end();
    }
 
    const_iterator end() const {
        return oscDataHierarchy_.end();
    }

    const GridType& grid() const {
        return grid_;
    }

    int size() const {
        return oscDataHierarchy_.size();
    }
};

#endif
