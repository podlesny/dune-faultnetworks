#ifndef OSC_UNITCUBE_ALUGRID_HH
#define OSC_UNITCUBE_ALUGRID_HH

#include "oscunitcube.hh"

#if HAVE_ALUGRID
#include <dune/grid/alugrid.hh>
#include <dune/grid/alugrid/3d/alu3dgridfactory.hh>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/grid/utility/structuredgridfactory.hh>

// ALU3dGrid and ALU2dGrid simplex specialization.
// Note: element type determined by type 
template<int dim>
class OscUnitCube<Dune::ALUGrid<dim, dim, Dune::simplex, Dune::nonconforming>, 2>
{
public:
  typedef Dune::ALUGrid<dim ,dim ,Dune::simplex, Dune::nonconforming> GridType;
  //typedef Dune::ALUSimplexGrid<dim,dim> GridType;

private:
  Dune::shared_ptr<GridType> grid_;

public:
  UnitCube ()
  {
      Dune::FieldVector<typename GridType::ctype,dim> lowerLeft(0);
      Dune::FieldVector<typename GridType::ctype,dim> upperRight(1);
      Dune::array<unsigned int,dim> elements;
      std::fill(elements.begin(), elements.end(), 1);
      
      grid_ = Dune::StructuredGridFactory<GridType>::createSimplexGrid(lowerLeft, upperRight, elements);
  }

  GridType &grid ()
  {
    return *grid_;
  }
};
#endif

#endif
