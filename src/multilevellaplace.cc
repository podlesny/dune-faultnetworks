#include <config.h>

#include <cmath>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/istl/matrix.hh>
#include <dune/common/timer.hh>

// dune-istl includes
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/preconditioners.hh>

// dune-fufem includes
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/assemblers/assembler.hh>
#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>
#include <dune/fufem/assemblers/localassemblers/l2functionalassembler.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>

// dune-grid includes
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/common/mcmgmapper.hh>

// dune-solver includes
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/norms/twonorm.hh>
//#include <dune/solvers/norms/onenorm.hh>
#include <dune/faultnetworks/compressedmultigridtransfer.hh>

#include <dune/faultnetworks/faultfactories/oscunitcube.hh>
#include <dune/faultnetworks/utils/debugutils.hh>
#include <dune/faultnetworks/utils/matrixreader.hh>
#include <dune/faultnetworks/utils/vectorreader.hh>
#include <dune/faultnetworks/oscrhs.hh>
#include <dune/faultnetworks/preconditioners/faultpreconditioner.hh>
#include <dune/faultnetworks/solvers/osccgsolver.hh>
#include <dune/faultnetworks/dgmgtransfer.hh>


#include <dune/istl/superlu.hh>
#include <dune/istl/umfpack.hh>

const int dim = 2;
typedef double ctype;


typedef typename Dune::FieldVector<ctype, dim> VectorType;
typedef typename Dune::BitSetVector<1> BitVector;
typedef typename Dune::BCRSMatrix<Dune::FieldMatrix<ctype, 1, 1>> MatrixType;


#if HAVE_UG
typedef typename Dune::UGGrid<dim> GridType;
typedef OscUnitCube<GridType, 2> GridOb;
#else 
#error No UG!
#endif

static const int dimworld = GridType::dimensionworld ;
static const int dim = GridType::dimension;
typedef typename GridType::ctype ctype;

typedef typename GridType::LevelGridView GV;

typedef P1NodalBasis<GV, ctype> Basis;
typedef typename Basis::LocalFiniteElement FE;
typedef CompressedMultigridTransfer<Dune::BlockVector<Dune::FieldVector<ctype,1>>> MGTransfer;

void iterate(MatrixType& coarseMat, MatrixType& fineMat, MGTransfer transfer,)
{
    preprocessCalled = false;

    int& level = this->level_;

    // Define references just for ease of notation
    std::vector<std::shared_ptr<const MatrixType> > const &mat = this->matrixHierarchy_;
    std::vector<std::shared_ptr<VectorType> >& x   = this->xHierarchy_;
    std::vector<VectorType>& rhs = this->rhsHierarchy_;

    // Solve directly if we're looking at the coarse problem
    if (level == 0) {
        basesolver_->solve();
        return;
    }

    // Presmoothing

    presmoother_[level]->setProblem(*(this->matrixHierarchy_[level]), *x[level], rhs[level]);
    presmoother_[level]->ignoreNodes_ = ignoreNodesHierarchy_[level];

    for (int i=0; i<this->nu1_; i++)
        presmoother_[level]->iterate();

    // /////////////////////////
    // Restriction

    // compute residual
    // fineResidual = rhs[level] - mat[level] * x[level];
    VectorType fineResidual = rhs[level];
    mat[level]->mmv(*x[level], fineResidual);

    // restrict residual
    this->mgTransfer_[level-1]->restrict(fineResidual, rhs[level-1]);


    // Set Dirichlet values.
    GenericVector::truncate(rhs[level-1], *ignoreNodesHierarchy_[level-1]);

    // Choose all zeros as the initial correction
    *x[level-1] = 0;

    // ///////////////////////////////////////
    // Recursively solve the coarser system
    level--;
    for (int i=0; i<mu_; i++)
        iterate();
    level++;

    // ////////////////////////////////////////
    // Prolong

    // add correction to the presmoothed solution
    VectorType tmp;
    this->mgTransfer_[level-1]->prolong(*x[level-1], tmp);
    *x[level] += tmp;

    // Postsmoothing
    postsmoother_[level]->setProblem(*(mat[level]), *x[level], rhs[level]);
    postsmoother_[level]->ignoreNodes_ = ignoreNodesHierarchy_[level];

    for (int i=0; i<this->nu2_; i++)
        postsmoother_[level]->iterate();

//    *(this->x_) = xHierarchy_.back();

}

int main(int argc, char** argv) try
{
 
  // parse parameter file
    ParameterTree parameterSet;
    if (argc==2)
        ParameterTreeParser::readINITree(argv[1], parameterSet);
    else
        ParameterTreeParser::readINITree("/home/mi/podlesny/software/dune/faultnetworks/src/multilevellaplace.parset", parameterSet);

    const std::string path = parameterSet.get<std::string>("path");
    const std::string resultPath = parameterSet.get<std::string>("resultPath");

    int problemCount = 0;
    while (parameterSet.hasSub("problem" + std::to_string(problemCount))) {
	
    ParameterTree& problemParameters = parameterSet.sub("problem" + std::to_string(problemCount));

    const int coarseResolution = problemParameters.get<int>("coarseResolution");
    const int fineResolution = problemParameters.get<int>("fineResolution");
    const int exactResolution = problemParameters.get<int>("exactResolution");

    const int maxIterations = problemParameters.get<int>("maxIterations");
    const double solverTolerance = problemParameters.get<double>("tol");

    const int fineGridN = std::pow(2, fineResolution);

    std::ofstream out(resultPath + "_" + std::to_string(coarseResolution) + "_" + std::to_string(fineResolution) + "_" + std::to_string(exactResolution) + ".log");
    std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
    std::cout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!


    std::cout << std::endl;
    std::cout << "Parameter file read successfully!" << std::endl;

    typedef Dune::Matrix<Dune::FieldMatrix<ctype,1,1>, std::allocator<Dune::FieldMatrix<ctype,1,1>>> OscMatrixType;
    OscMatrixType matrix;

    const int oscGridN = matrix.N();


    Dune::UGGrid<dim>::setDefaultHeapSize(4000);
    GridOb unitCube(coarseGridN_);
    GridType& grid = *(unitCube.grid());

    const int fineLevelIdx = fineResolution_ - coarseResolution_;
    const int exactLevelIdx = exactResolution_ - coarseResolution_;
    grid_->globalRefine(exactLevelIdx);

    std::cout << "Coarse and fine grid were generated!" << std::endl;

    typedef  typename GV::Codim <0>::Iterator  ElementLevelIterator;
    typedef  typename ElementLevelIterator::Entity::Geometry  LevelGeometry;
    int i,j;
    
    Timer totalTimer;
    totalTimer.reset();
    totalTimer.start();
 // ----------------------------------------------------------------
    // ---              compute initial iterate                     ---
    // ----------------------------------------------------------------
    std::cout << std::endl;
    std::cout << "Computing initial iterate!" << std::endl;
 
    GV coarseGridView(grid.levelGridView(0));
    GV fineGridView(grid.levelGridView(fineLevelIdx));
    DGBasis fineBasis(fineGridView, fineFaultNetwork);

       
    // assemble stiffness matrix
    Basis coarseBasis(coarseGridView);

    MatrixType coarseStiffMat;
    Assembler<Basis, Basis> coarseAssembler(coarseBasis, coarseBasis);
    LaplaceAssembler<GridType, FE, FE> coarseLocalAssembler();
    coarseAssembler.assembleOperator(coarseLocalAssembler, coarseStiffMat);

    // assemble rhs
    Dune::BlockVector<Dune::FieldVector<double,1>> coarseRhs, coarseSol, contCoarseSol, initialX;
    coarseRhs.resize(coarseStiffMat.N());
    coarseSol.resize(coarseStiffMat.M());

    OscRhs<VectorType, Dune::FieldVector<ctype,1>> f;
    L2FunctionalAssembler<GridType, FE> l2FunctionalAssembler(f);
    coarseAssembler.assembleFunctional(l2FunctionalAssembler, coarseRhs);

    typedef typename MatrixType::row_type RowType;
    typedef typename RowType::ConstIterator ColumnIterator;

    // set boundary conditions
    BitVector coarseBoundaryDofs;
    BoundaryPatch<GridType::LevelGridView> coarseBoundaryPatch(coarseGridView, true);
    constructBoundaryDofs(coarseBoundaryPatch, coarseBasis, coarseBoundaryDofs);

    for(size_t i=0; i<coarseBoundaryDofs.size(); i++) {
        if(!coarseBoundaryDofs[i][0])           
		continue;

        const RowType& row = coarseStiffMat[i];

        ColumnIterator cIt    = row.begin();
        ColumnIterator cEndIt = row.end();

        for(; cIt!=cEndIt; ++cIt) {
            coarseStiffMat[i][cIt.index()] = 0;
        }

        coarseStiffMat[i][i]=1;
        coarseRhs[i] = 0;
        coarseSol[i] = 0;
    }


    // compute coarse solution
    Timer timer;
    timer.reset();
    timer.start();
    #if HAVE_SUPERLU
    Dune::InverseOperatorResult res;
    Dune::SuperLU<MatrixType> coarseSolver(coarseStiffMat);
    coarseSolver.apply(coarseSol, coarseRhs, res);
    #else
    #error No SuperLU!
    #endif
    std::cout << "Solving coarse problem: " << timer.elapsed() << " seconds" << std::endl;
    timer.stop();
    timer.reset();	

    CompressedMultigridTransfer<Dune::BlockVector<Dune::FieldVector<ctype,1>>> mgTransfer;
    mgTransfer.setup(coarseGridView, fineGridView);
    mgTransfer.prolong(coarseSol, contCoarseSol);

    //prolong to discontinous fineLevel basis
    fineBasis.prolong(contCoarseSol, initialX);


    // ----------------------------------------------------------------
    // ---              compute exact solution                      ---
    // ----------------------------------------------------------------
    std::cout << std::endl;
    std::cout << "Computing exact solution!" << std::endl;
 
    GV exactGridView(grid.levelGridView(exactLevelIdx));
    Basis exactBasis(exactGridView);

    // assemble stiffness matrix
    MatrixType exactStiffMat;
    Assembler<Basis, Basis> exactAssembler(exactBasis, exactBasis);
    LaplaceAssembler<GridType, FE, FE> oscExactAssembler();
    exactAssembler.assembleOperator(oscExactAssembler, exactStiffMat);

    // assemble rhs
    Dune::BlockVector<Dune::FieldVector<double,1>> rhs, exactSol;
    rhs.resize(exactStiffMat.N());
    exactSol.resize(exactStiffMat.M());

    exactAssembler.assembleFunctional(l2FunctionalAssembler, rhs);

    // set boundary conditions
    BitVector boundaryDofs;
    BoundaryPatch<GridType::LevelGridView> boundaryPatch(exactGridView, true);
    constructBoundaryDofs(boundaryPatch, exactBasis, boundaryDofs);

    for(size_t i=0; i<boundaryDofs.size(); i++) {
            if(!boundaryDofs[i][0])
                continue;

            RowType& row = exactStiffMat[i];

            ColumnIterator cIt    = row.begin();
            ColumnIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt) {
                row[cIt.index()] = 0;
            }

            row[i]=1;
            rhs[i] = 0;
            exactSol[i] = 0;
     }

    // compute exact solution

    if (useExactSol) {
        VectorReader<Dune::BlockVector<Dune::FieldVector<double,1>> > vectorReader(resultPath + exactSolFile);
        vectorReader.read(exactSol);

        if (exactSol.size()!=exactBasis.size())
            DUNE_THROW(Exception, "The provided exact solution has wrong dimensions!");
    } else {

        timer.start();
        /*#if HAVE_UMFPACK
        Dune::UMFPack<MatrixType> exactSolver(exactStiffMat);
        exactSolver.apply(exactSol, rhs, res);
        #else
        #error No UMFPack!
        #endif*/

        #if HAVE_SUPERLU
        Dune::SuperLU<MatrixType> exactSolver(exactStiffMat, false);
        exactSolver.setVerbosity(false);
        exactSolver.apply(exactSol, rhs, res);
        #else
        #error No SuperLU!
        #endif

        std::cout << "Solving exact problem: " << timer.elapsed() << " seconds" << std::endl;
        timer.stop();
        timer.reset();
    }

    EnergyNorm<MatrixType, Dune::BlockVector<Dune::FieldVector<ctype,1>> > exactEnergyNorm(exactStiffMat);


    // ----------------------------------------------------------------
    // ---                set up cg solver                          ---
    // ----------------------------------------------------------------
    std::cout << std::endl;
    std::cout << "---------------------------------------------" << std::endl; 
    std::cout << "Setting up the fine level CG solver:" << std::endl; 
    std::cout << "---------------------------------------------" << std::endl << std::endl;


    std::cout << "Assembling stiffness matrix" << std::endl;
    

    // assemble stiffness matrix
    MatrixType fineStiffMat;
    Assembler<Basis, Basis> fineAssembler(fineBasis, fineBasis);
    LaplaceAssembler<GridType, FE, FE> oscFineAssembler();
    fineAssembler.assembleOperator(oscFineAssembler, fineStiffMat);

    std::cout << "Assembling right hand side" << std::endl;
    Dune::BlockVector<Dune::FieldVector<double,1>> fineRhs, fineX;
    fineRhs.resize(fineStiffMat.N());
    fineX.resize(fineStiffMat.M());

    fineAssembler.assembleFunctional(l2FunctionalAssembler, fineRhs);
    
    std::cout << "Setting boundary conditions" << std::endl;
    // set boundary conditions
    BitVector fineBoundaryDofs;
    BoundaryPatch<GridType::LevelGridView> fineBoundaryPatch(fineGridView, true);
    constructBoundaryDofs(fineBoundaryPatch, fineBasis, fineBoundaryDofs);

    for(size_t i=0; i<fineBoundaryDofs.size(); i++) {
        if(!fineBoundaryDofs[i][0])
            continue;

        RowType& row = fineStiffMat[i];

        ColumnIterator cIt    = row.begin();
        ColumnIterator cEndIt = row.end();

        for(; cIt!=cEndIt; ++cIt) {
            row[cIt.index()] = 0;
        }

        row[i] = 1;
        fineRhs[i] = 0;
	fineX[i] = 0;
	initialX[i] = 0;
    }

    // compute exact solution on fine grid
    std::cout << "Computing the exact solution directly" << std::endl;
    #if HAVE_SUPERLU
    Dune::InverseOperatorResult opRes;
    Dune::BlockVector<Dune::FieldVector<double,1>> fineRhsClone(fineRhs);

    Dune::SuperLU<MatrixType> fineSolver(fineStiffMat);
    fineSolver.apply(fineX, fineRhsClone, opRes);
    #else
    #error No SuperLU!
    #endif

    EnergyNorm<MatrixType, Dune::BlockVector<Dune::FieldVector<ctype,1>> > energyNorm(fineStiffMat);

    std::cout << "Setting up the preconditioner!" << std::endl;
    FaultPreconditioner<GridType, Basis, DGBasis, MatrixType, Dune::BlockVector<Dune::FieldVector<ctype,1>> > preconditioner(grid, 0, fineLevelIdx, fineOscData, fineMapper, fineStiffMat, fineRhs, fineFaultNetwork, false);

    std::cout << "Setup complete, starting preconditioned cg iteration!" << std::endl;
    std::cout << std::endl << std::endl;
    
/*    Dune::BlockVector<Dune::FieldVector<ctype,1>> initialError(fineX.size());
    initialError = initialX;
    initialError -= fineX;
    std::cout << "Initial error in energy norm: " << energyNorm.operator()(initialError) << std::endl;
    std::cout << "Initial error in two norm: " << initialError.two_norm() << std::endl << std::endl;*/

    Dune::BlockVector<Dune::FieldVector<ctype,1>> initialXCopy(initialX);
    Dune::BlockVector<Dune::FieldVector<ctype,1>> fineRhsCopy(fineRhs);


    //print(initialX, "initialX");
    //print(fineRhs, "fineRhs");
    //print(exactSol, "exactSol");
    //print(fineX, "fineX");
    
   /* Basis fineContBasis(fineGridView);
    std::cout << "fineContBasis.size(): " << fineContBasis.size() << std::endl;
    DGMGTransfer<Basis> contMGTransfer(coarseBasis, fineContBasis);

    Basis exactContBasis(exactGridView);
    std::cout << "exactContBasis.size(): " << exactContBasis.size() << std::endl; */
    DGMGTransfer<DGBasis> discMGTransfer(fineBasis, exactBasis);

    // solve
    OscCGSolver<MatrixType, Dune::BlockVector<Dune::FieldVector<ctype,1>>, DGBasis>
    solver(fineBasis, &fineStiffMat, &initialX, &fineRhs, &preconditioner,
                    maxIterations, solverTolerance, &exactEnergyNorm,
                    Solver::FULL, &exactSol, &fineX, discMGTransfer, 1.0, true); //((oscGridN+0.0)/fineGridN)
    solver.check();
    solver.preprocess();
    solver.solve();

    Dune::BlockVector<Dune::FieldVector<ctype,1>> finalError(fineX.size());
    finalError = initialX;
    finalError -= fineX;
    std::cout << "Final error in energy norm: " << energyNorm.operator()(finalError) << std::endl;
    std::cout << "Final error in two norm: " << finalError.two_norm() << std::endl << std::endl;

    std::cout << std::endl;
    std::cout << "Total time: " << totalTimer.elapsed() << " seconds." << std::endl; 

    // regular unpreconditioned cg solver for comparisson

    std::cout << "Setup complete, starting cg iteration!" << std::endl;
    std::cout << std::endl;
    
    // solve
    OscCGSolver<MatrixType, Dune::BlockVector<Dune::FieldVector<ctype,1>>, DGBasis >
    regSolver(fineBasis, &fineStiffMat, &initialXCopy, &fineRhsCopy, nullptr,
                    maxIterations, solverTolerance, &exactEnergyNorm,
                    Solver::FULL, &exactSol, &fineX, discMGTransfer, 1.0, true);
    regSolver.check();
    regSolver.preprocess();
    regSolver.solve();

    // TODO: use once debugging finished
    std::cout.rdbuf(coutbuf); //reset to standard output again

    std::cout << "Problem " << problemCount << " done!" << std::endl;
    problemCount++;
    }
} catch (Dune::Exception e) {

    std::cout << e << std::endl;

}
