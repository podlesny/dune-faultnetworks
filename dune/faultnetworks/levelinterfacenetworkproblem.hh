#ifndef GLOBAL_PROBLEM_ASSEMBLER_HH
#define GLOBAL_PROBLEM_ASSEMBLER_HH

#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>

#include <dune/faultnetworks/assemblers/globalfaultassembler.hh>
#include <dune/faultnetworks/interfacenetwork.hh>
#include <dune/faultnetworks/levelinterfacenetwork.hh>
#include <dune/faultnetworks/faultinterface.hh>
#include <dune/faultnetworks/oscdata.hh>

#include <dune/istl/umfpack.hh>

#include <dune/faultnetworks/utils/debugutils.hh>

template <class DGBasis, class MatrixType, class VectorType>
class LevelInterfaceNetworkProblem
{

    private:
        typedef typename DGBasis::GridView GridView;
        typedef typename GridView::Grid GridType;

        const LevelInterfaceNetwork<GridView>& levelInterfaceNetwork_;

        std::shared_ptr<DGBasis> basis_;

        MatrixType matrix_;
        VectorType rhs_;
        VectorType x_;

        bool requireAssemble_;

    public:
        LevelInterfaceNetworkProblem(const LevelInterfaceNetwork<GridView>& levelInterfaceNetwork) :
            levelInterfaceNetwork_(levelInterfaceNetwork),
            basis_(std::make_shared<DGBasis>(levelInterfaceNetwork_)),
            requireAssemble_(true) {}


        template <class LocalAssembler, class LocalInterfaceAssembler, class LocalFunctionalAssembler>
        void assemble(LocalAssembler& localAssembler, const std::vector<std::shared_ptr<LocalInterfaceAssembler>>& localInterfaceAssemblers, LocalFunctionalAssembler& localFunctionalAssembler) {
            if (requireAssemble_) {

                GlobalFaultAssembler<DGBasis, DGBasis> globalFaultAssembler(*basis_, *basis_, levelInterfaceNetwork_);
                globalFaultAssembler.assembleOperator(localAssembler, localInterfaceAssemblers, matrix_);
                globalFaultAssembler.assembleFunctional(localFunctionalAssembler, rhs_);

                x_.resize(matrix_.M());

                typedef typename MatrixType::row_type RowType;
                typedef typename RowType::ConstIterator ColumnIterator;

                const GridView& gridView = levelInterfaceNetwork_.levelGridView();

                // set boundary conditions
                Dune::BitSetVector<1> boundaryDofs;
                BoundaryPatch<GridView> boundaryPatch(gridView, true);
                constructBoundaryDofs(boundaryPatch, *basis_, boundaryDofs);

                for(size_t i=0; i<boundaryDofs.size(); i++) {
                    if(!boundaryDofs[i][0])
                        continue;

                    RowType& row = matrix_[i];

                    ColumnIterator cIt    = row.begin();
                    ColumnIterator cEndIt = row.end();

                    for(; cIt!=cEndIt; ++cIt) {
                        row[cIt.index()] = 0;
                    }

                    row[i] = 1;
                    rhs_[i] = 0;
                    x_[i] = 0;
                }

                requireAssemble_ = false;
            }
        }

        void solve() {
            // compute solution directly

            if (requireAssemble_)
                DUNE_THROW(Dune::Exception, "GlobalProblemAssembler::solve() Call assemble() before solving the global problem!");

            Dune::Timer timer;
            timer.reset();
            timer.start();

            #if HAVE_UMFPACK
            Dune::InverseOperatorResult res;
            VectorType rhsCopy(rhs_);

            Dune::UMFPack<MatrixType> solver(matrix_);
            solver.apply(x_, rhsCopy, res);
            #else
            #error No UMFPack!
            #endif

            std::cout << "Solving global problem took: " << timer.elapsed() << " seconds" << std::endl;
        }

        const MatrixType& matrix() const {
            if (requireAssemble_)
                DUNE_THROW(Dune::Exception, "GlobalProblemAssembler::matrix() Call assemble() before accessing matrix()!");
            else
                return matrix_;
        }

        const VectorType& rhs() const {
            if (requireAssemble_)
                DUNE_THROW(Dune::Exception, "GlobalProblemAssembler::rhs() Call assemble() before accessing rhs()!");
            else
                return rhs_;
        }

        const VectorType& getSol() const {
            return x_;
        }

        std::shared_ptr<DGBasis> basis() const {
            if (requireAssemble_)
                DUNE_THROW(Dune::Exception, "GlobalProblemAssembler::basis() Call assemble() before accessing basis()!");
            else
                return basis_;
        }

        const LevelInterfaceNetwork<GridView>& levelInterfaceNetwork() const {
            return levelInterfaceNetwork_;
        }
};

#endif

