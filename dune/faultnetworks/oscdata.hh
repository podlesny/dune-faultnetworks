#ifndef OSC_DATA_HH
#define OSC_DATA_HH

#include <dune/grid/common/mcmgmapper.hh>
#include <dune/faultnetworks/hierarchicleveliterator.hh>

template <class GridType, class MatrixType>
class OscData {
private:
    static const int dim = GridType::dimensionworld;

    typedef typename GridType::ctype ctype;
    typedef typename Dune::LevelMultipleCodimMultipleGeomTypeMapper<GridType, Dune::MCMGElementLayout> MapperType;
    typedef typename GridType::LevelGridView GridView;
    typedef typename GridView::template Codim <0>::Iterator  ElementLevelIterator;

    const GridType& grid_;
    const int level_;
    const GridView gridView_;

    std::vector<ctype> data_;
    MapperType mapper_;

    bool isSet_ = false;

    void prolong(OscData<GridType, MatrixType>& fineOscData) const {
        typedef typename GridType::template Codim<0>::Entity ElementType;

        const GridView& fineGridView = fineOscData.gridView();
        const MapperType& fineMapper = fineOscData.mapper();

        std::vector<ctype> fineData(fineOscData.size());

        ElementLevelIterator endIter = fineGridView.template end <0>();
        for (ElementLevelIterator  it = fineGridView.template begin <0>(); it!=endIter; ++it) {
            ElementType father = *it;

            while (father.level() > level_) {
                father = father.father();
            }

            assert(gridView_.contains(father));

            fineData[fineMapper.index(*it)] = data_[mapper_.index(father)];
        }

        fineOscData.set(fineData);
    }

    void restriction(OscData<GridType, MatrixType>& coarseOscData) const {
        const GridView& coarseGridView = coarseOscData.gridView();
        const MapperType& coarseMapper = coarseOscData.mapper();

        std::vector<ctype> coarseData(coarseMapper.size());
 
        typedef HierarchicLevelIterator<GridType> HierarchicLevelIteratorType;
        ElementLevelIterator endElemIt = coarseGridView.template end <0>();
        for (ElementLevelIterator  it = coarseGridView.template begin <0>(); it!=endElemIt; ++it) {
            int counter = 0;
            ctype sum = 0;
 	  
            HierarchicLevelIteratorType endHierIt(grid_, *it, HierarchicLevelIteratorType::PositionFlag::end, level_);
            for (HierarchicLevelIteratorType hierIt(grid_, *it, HierarchicLevelIteratorType::PositionFlag::begin, level_); hierIt!=endHierIt; ++hierIt) {
                sum += data_[mapper_.index(*hierIt)];
                counter++;
            }

            coarseData[coarseMapper.index(*it)] = sum/counter;
        }

        coarseOscData.set(coarseData);
    }

public:
        OscData(const GridType& grid, const int level) : grid_(grid), level_(level), gridView_(grid_.levelGridView(level_)), mapper_(grid_, level_) {
            data_.resize(mapper_.size());
        }
          
        void set(const MatrixType& matrix) {
            typedef typename ElementLevelIterator::Entity::Geometry LevelGeometry;
            int i,j;

            const int matrixN = matrix.N();
            const int matrixM = matrix.M();

            ElementLevelIterator endIt = gridView_.template end <0>();
            for (ElementLevelIterator  it = gridView_.template begin <0>(); it!=endIt; ++it) {
                const LevelGeometry  geometry = it->geometry();

                Dune::FieldVector <ctype, dim > global = geometry.center();
                i = matrixN - std::ceil(global[0]*matrixN);
                j = std::floor(global[1]*matrixM);
                data_[mapper_.index(*it)] = matrix[i][j];
            }

            isSet_ = true;
        }

        void set(const std::vector<ctype>& data) {
            if (data.size() == data_.size()) {
                data_ = data;
                isSet_ = true;
            } else {
                DUNE_THROW(Dune::Exception, "OscData.set(): input data has wrong size!");
            }
        }

        void adapt(OscData<GridType, MatrixType>& newOscData) const {
            const int newLevel = newOscData.level();

            if (newLevel>level_) {
                prolong(newOscData);
            } else if (newLevel<level_) {
                restriction(newOscData);
            } else {
                newOscData.set(data_);
            }
        }

        bool isSet() const {
            return isSet_;
        }

        const GridType& grid() const {
            return grid_;
        }

        int level() const {
            return level_;
        }

        const GridView& gridView() const {
            return gridView_;
        }

        int size() const {
            return data_.size();
        }

        const MapperType& mapper() const {
            return mapper_;
        }

        const std::vector<ctype>& data() const {
            return data_;
        }
};

#endif
