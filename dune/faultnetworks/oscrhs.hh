#ifndef OSC_RHS_HH
#define OSC_RHS_HH

#include <dune/common/function.hh>

template <class DomainType, class RangeType> 
class OscRhs : public Dune::VirtualFunction<DomainType, RangeType> {
    public:
        OscRhs(){};
          
   //     void evaluate(const typename Dune::VirtualFunction<DomainType, RangeType>::Traits::DomainType& x, typename Dune::VirtualFunction<DomainType, RangeType>::Traits::RangeType& y) {
        void evaluate(const DomainType& x, RangeType& y) const {     
            y = 1;
        }
};

#endif
