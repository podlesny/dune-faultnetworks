#ifndef INTERFACE_NETWORK_HH
#define INTERFACE_NETWORK_HH


#include <dune/common/bitsetvector.hh>
#include <dune/common/fvector.hh>

#include <dune/faultnetworks/facehierarchy.hh>
#include <dune/faultnetworks/faultinterface.hh>
#include <dune/faultnetworks/levelinterfacenetwork.hh>

template <class GridType>
class InterfaceNetwork
{
    protected:
        typedef typename GridType::LevelGridView GridView;
        typedef typename GridType::LevelIntersection Intersection;
        typedef typename GridType::ctype ctype;
        static const int dim = GridType::dimension;
				
    private:
        const GridType& grid_;
        std::vector<std::shared_ptr<LevelInterfaceNetwork<GridView>>> levelInterfaceNetworks_;

	public:
        InterfaceNetwork(const GridType& grid) : grid_(grid) {

            // init levelInterfaceNetworks
            levelInterfaceNetworks_.resize(grid.maxLevel()+1);

            for (size_t i=0; i<levelInterfaceNetworks_.size(); i++) {
                levelInterfaceNetworks_[i] = std::make_shared<LevelInterfaceNetwork<GridView>>(grid_, i);
            }
        }

        size_t size() const {
            return levelInterfaceNetworks_.size();
        }

        size_t size(int level) const {
            return levelInterfaceNetworks_[level]->size();
        }

        /*
        const std::shared_ptr<FaultInterface<GridView>> getInterface(const size_t interfaceID, const int level) const {
            return levelInterfaceNetworks_[level]->getInterface(interfaceID);
        }*/

        void prolongInterface(const std::shared_ptr<FaultInterface<GridView>> interfacePtr, LevelInterfaceNetwork<GridView>& toLevelInterfaceNetwork) {

            const GridType& grid = interfacePtr->grid();
            const int toLevel = toLevelInterfaceNetwork.level();

            // init prolonged interfaces
            std::shared_ptr<FaultInterface<GridView>> newInterface = std::make_shared<FaultInterface<GridView>>(toLevelInterfaceNetwork.levelGridView(), toLevel, interfacePtr);

            // iterate over interface intersections
            const std::vector<Intersection>& faces = interfacePtr->faces();
            for (size_t i=0; i<faces.size(); i++) {
                const Intersection& intersection = faces[i];
                Face<GridType> face(grid, intersection.inside(), intersection.indexInInside());

                if (toLevel == interfacePtr->level()) {
                    newInterface->addFace(intersection);
                } else {
                    typename Face<GridType>::HierarchicIterator exactIt = face.hbegin(toLevel);
                    typename Face<GridType>::HierarchicIterator exactEnd = face.hend(toLevel);

                    for(; exactIt!=exactEnd; ++exactIt) {
                        if (exactIt->level() == toLevel) {
                            newInterface->addFace(*(exactIt->intersection()));
                        }
                    }
                }
            }

            // build new interface and add to levelInterfaceNetwork
            toLevelInterfaceNetwork.addInterface(newInterface);
        }

        void prolongInterface(const std::shared_ptr<FaultInterface<GridView>> interfacePtr, LevelInterfaceNetwork<GridView>& toLevelInterfaceNetwork) const {

            const GridType& grid = interfacePtr->grid();
            const int toLevel = toLevelInterfaceNetwork.level();

            // init prolonged interfaces
            std::shared_ptr<FaultInterface<GridView>> newInterface = std::make_shared<FaultInterface<GridView>>(toLevelInterfaceNetwork.levelGridView(), toLevel, interfacePtr);

            // iterate over interface intersections
            const std::vector<Intersection>& faces = interfacePtr->faces();
            for (size_t i=0; i<faces.size(); i++) {
                const Intersection& intersection = faces[i];
                Face<GridType> face(grid, intersection.inside(), intersection.indexInInside());

                if (toLevel == interfacePtr->level()) {
                    newInterface->addFace(intersection);
                } else {
                    typename Face<GridType>::HierarchicIterator exactIt = face.hbegin(toLevel);
                    typename Face<GridType>::HierarchicIterator exactEnd = face.hend(toLevel);

                    for(; exactIt!=exactEnd; ++exactIt) {
                        if (exactIt->level() == toLevel) {
                            newInterface->addFace(*(exactIt->intersection()));
                        }
                    }
                }
            }

            // build new interface and add to levelInterfaceNetwork
            toLevelInterfaceNetwork.addInterface(newInterface);
        }
        
        // CANNOT be used to copy interface to same level (prolong to level of interfacePtr->level())
        void prolongInterface(const std::shared_ptr<FaultInterface<GridView>> interfacePtr, const std::set<int> toLevels) {

            const GridType& grid = interfacePtr->grid();
            const int maxLevel = *(toLevels.rbegin());

            // init prolonged interfaces
            std::vector<std::shared_ptr<FaultInterface<GridView>>> newInterfaces(toLevels.size());
            std::set<int>::iterator it = toLevels.begin();
            for (size_t i=0; i<newInterfaces.size(); i++) {
                const int newLevel = *it;

                // set father interface appropriately
                if (i==0)
                    newInterfaces[i] = std::make_shared<FaultInterface<GridView>>(levelInterfaceNetworks_[newLevel]->levelGridView(), newLevel, interfacePtr);
                else
                    newInterfaces[i] = std::make_shared<FaultInterface<GridView>>(levelInterfaceNetworks_[newLevel]->levelGridView(), newLevel, newInterfaces[i-1]);

                it++;
            }

            // iterate over interface intersections
            const std::vector<Intersection>& faces = interfacePtr->faces();
            for (size_t i=0; i<faces.size(); i++) {
                const Intersection& intersection = faces[i];
                Face<GridType> face(grid, intersection.inside(), intersection.indexInInside());

                typename Face<GridType>::HierarchicIterator exactIt = face.hbegin(maxLevel);
                typename Face<GridType>::HierarchicIterator exactEnd = face.hend(maxLevel);
                for(; exactIt!=exactEnd; ++exactIt) {
                        std::set<int>::iterator it = toLevels.find(exactIt->level());
                        if (it != toLevels.end()) {
                            newInterfaces[std::distance(toLevels.begin(), it)]->addFace(*(exactIt->intersection()));
                        }
                }
            }

            // build new interfaces and add to levelInterfaceNetworks
            for (size_t i=0; i<newInterfaces.size(); i++) {
                std::shared_ptr<FaultInterface<GridView>> newInterface = newInterfaces[i];
                levelInterfaceNetworks_[newInterface->level()]->addInterface(newInterface);
            }
        }

        void prolongLevel(const int level, const int toLevel) {
            if (level<toLevel) {

                std::shared_ptr<LevelInterfaceNetwork<GridView>>& levelInterfaceNetworkPtr = levelInterfaceNetworks_[level];
                std::shared_ptr<LevelInterfaceNetwork<GridView>>& newLevelInterfaceNetworkPtr = levelInterfaceNetworks_[toLevel];

                const GridType& grid = levelInterfaceNetworkPtr->grid();

                // iterate over interface intersections
                const std::vector<Intersection>& faces = levelInterfaceNetworkPtr->getIntersections();
                const std::vector<int>& facesLevels = levelInterfaceNetworkPtr->getIntersectionsLevels();
                for (size_t i=0; i<faces.size(); i++) {
                    const Intersection& intersection = faces[i];
                    const int intersectionLevel = facesLevels[i];
                    Face<GridType> face(grid, intersection.inside(), intersection.indexInInside());

                    typename Face<GridType>::HierarchicIterator exactIt = face.hbegin(toLevel);
                    typename Face<GridType>::HierarchicIterator exactEnd = face.hend(toLevel);

                    for(; exactIt!=exactEnd; ++exactIt) {
                        if (exactIt->level() == toLevel) {
                            newLevelInterfaceNetworkPtr->addIntersection(*(exactIt->intersection()), intersectionLevel);
                        }
                    }
                }
            }
        }
        
        void prolongLevels(const int level, const std::set<int> toLevels) {
            const int minLevel = *(toLevels.begin());
            const int maxLevel = *(toLevels.rbegin());

            if (level<minLevel) {
                std::shared_ptr<LevelInterfaceNetwork<GridView>>& levelInterfaceNetworkPtr = levelInterfaceNetworks_[level];

                const GridType& grid = levelInterfaceNetworkPtr->grid();

                // iterate over interface intersections
                const std::vector<Intersection>& faces = levelInterfaceNetworkPtr->getIntersections();
                const std::vector<int>& facesLevels = levelInterfaceNetworkPtr->getIntersectionsLevels();
                for (size_t i=0; i<faces.size(); i++) {
                    const Intersection& intersection = faces[i];
                    const int intersectionLevel = facesLevels[i];
                    Face<GridType> face(grid, intersection.inside(), intersection.indexInInside());

                    typename Face<GridType>::HierarchicIterator exactIt = face.hbegin(maxLevel);
                    typename Face<GridType>::HierarchicIterator exactEnd = face.hend(maxLevel);

                    for(; exactIt!=exactEnd; ++exactIt) {
                        std::set<int>::iterator it = toLevels.find(exactIt->level());
                        if (it != toLevels.end()) {
                            levelInterfaceNetworks_[*it]->addIntersection(*(exactIt->intersection()), intersectionLevel);
                        }
                    }
                }
            }
        }



        /*void prolongLevelInterfaces(const int level, LevelInterfaceNetwork<GridView>& toLevelInterfaceNetwork) const {
            const std::shared_ptr<LevelInterfaceNetwork<GridView>>& levelInterfaceNetworkPtr = levelInterfaceNetworks_[level];
            levelInterfaceNetworkPtr->build();

            for (size_t i=0; i<levelInterfaceNetworkPtr->size(); i++) {
                prolongInterface(levelInterfaceNetworkPtr->getInterface(i), toLevelInterfaceNetwork);
            }
        }*/

        void addInterface(std::shared_ptr<FaultInterface<GridView>>& interface){
            const int level = interface->level();
            levelInterfaceNetworks_[level]->addInterface(interface);
        }

        void addLevelInterfaceNetwork(std::shared_ptr<LevelInterfaceNetwork<GridView>> interfaceNetwork){
            const int level = interfaceNetwork->level();
            levelInterfaceNetworks_[level] = interfaceNetwork;
        }

    
        size_t localIndex(const size_t i, const int level) const {
            return levelInterfaceNetworks_[level]->localIndex(i);
        }
	
        size_t dofCount(const int level) const {
            return levelInterfaceNetworks_[level]->dofCount();
        }

        const std::set<size_t>& getInterfaceNetworkDofs(const int level) const{
            return levelInterfaceNetworks_[level]->getInterfaceNetworkDofs();
        }

        int maxLevel() const {
            return levelInterfaceNetworks_.size();
        }

        const LevelInterfaceNetwork<GridView>& levelInterfaceNetwork(const int i) const {
            return *(levelInterfaceNetworks_[i]);
        }

        std::shared_ptr<LevelInterfaceNetwork<GridView>> levelInterfaceNetworkPtr(const int i) const {
            return levelInterfaceNetworks_[i];
        }

        const GridType& grid() {
            return grid_;
        }

        const GridView& levelGridView(const int i) const {
            return levelInterfaceNetworks_[i]->levelGridView();
        }
};

#endif
