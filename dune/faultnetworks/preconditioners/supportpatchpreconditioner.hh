#ifndef SUPPORT_PATCH_PRECONDITIONER_HH
#define SUPPORT_PATCH_PRECONDITIONER_HH

#include <string>

#include <dune/faultnetworks/preconditioners/levelpatchpreconditioner.hh>
#include <dune/faultnetworks/patchfactories/supportpatchfactory.hh>

#include <dune/faultnetworks/utils/debugutils.hh>

template <class BasisType, class LocalAssembler, class LocalInterfaceAssembler, class MatrixType, class VectorType>
class SupportPatchPreconditioner : public LevelPatchPreconditioner<BasisType, LocalAssembler, LocalInterfaceAssembler, MatrixType, VectorType> {
private:
    using Base = LevelPatchPreconditioner<BasisType, LocalAssembler, LocalInterfaceAssembler, MatrixType, VectorType>;
    using GridView = typename Base::GridView;
    using GridType = typename Base::GridType;
    using Mode = typename Base::Mode;

public:
    // for each coarse patch given by patchLevelBasis: set up local problem, compute local correction
    SupportPatchPreconditioner(const LevelInterfaceNetwork<GridView>& levelInterfaceNetwork,
                             const BasisType& patchLevelBasis,
                             const LocalAssembler& localAssembler,
                             const std::vector<std::shared_ptr<LocalInterfaceAssembler>>& localInterfaceAssemblers,
                             const Mode mode = Mode::ADDITIVE) :
          Base(levelInterfaceNetwork, patchLevelBasis, localAssembler, localInterfaceAssemblers, mode)
    {}

    void build() {
        // init vertexInElements
        const int dim = GridType::dimension;
        using Element = typename GridType::template Codim<0>::Entity;
        std::vector<std::vector<Element>> vertexInElements;

        const GridView& patchLevelGridView = this->patchLevelBasis_.getGridView();
        vertexInElements.resize(patchLevelGridView.size(dim)); //(this->patchLevelBasis_.size());
        for (size_t i=0; i<vertexInElements.size(); i++) {
            vertexInElements[i].resize(0);
        }

        for (const auto& elem : elements(patchLevelGridView)) {
            // compute coarseGrid vertexInElements
            const auto& coarseFE = this->patchLevelBasis_.getLocalFiniteElement(elem);

            for (size_t i=0; i<coarseFE.localBasis().size(); ++i) {
                int dofIndex = this->patchLevelBasis_.indexInGridView(elem, i); //index
                vertexInElements[dofIndex].push_back(elem);
            }
        }

        std::cout << "SupportPatchPreconditioner::build() level: " << this->level_ << std::endl;

        //printBasisDofLocation(this->patchLevelBasis_);

        this->localProblems_.resize(vertexInElements.size());
        const int patchLevel = this->patchLevelBasis_.faultNetwork().level();
        for (size_t i=0; i<vertexInElements.size(); i++) {
            SupportPatchFactory<BasisType> patchFactory(this->patchLevelBasis_, this->basis_, patchLevel, this->level_, vertexInElements, i, this->patchDepth_);
            const auto& localToGlobal = patchFactory.getLocalToGlobal();
            const auto& boundaryDofs = patchFactory.getBoundaryDofs();

            VectorType rhs;
            rhs.resize(this->matrix_.N());
            rhs = 0;

            using LocalProblemType = typename std::remove_reference<decltype(*(this->localProblems_[i]))>::type;
            this->localProblems_[i] = std::make_shared<LocalProblemType>(this->matrix_, rhs, localToGlobal, boundaryDofs);
        }
    }
};

#endif

