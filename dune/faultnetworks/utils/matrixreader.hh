#ifndef FAULTNETWORKS_MATRIX_READER_HH
#define FAULTNETWORKS_MATRIX_READER_HH

#include <fstream>

// dune-common

#include <dune/common/exceptions.hh>





using namespace Dune;


template <class MatrixType>
class MatrixReader {
    private: 
        std::string filePath_;

        void split(const std::string& str, char delimiter, std::vector<std::string>& elements) {
            std::stringstream strStream(str);
            std::string token;
            
            while (std::getline(strStream, token, delimiter)) {
                elements.push_back(token);
            }
        }

     
    public: 
        typedef MatrixType matrix_type;

        void setSource(const std::string filePath) {
            filePath_ = filePath;
        }


        MatrixReader(const std::string filePath) {
            filePath_ = filePath;
        }

        
        void read(MatrixType& matrix) {
            std::string line;
            std::vector<std::string> lineSplit;
            std::ifstream file (filePath_);

            if (file.is_open()) {

                getline (file,line);
                split(line, ' ', lineSplit);

                size_t rows = atoi(lineSplit[0].c_str());
                size_t cols = atoi(lineSplit[1].c_str());

		matrix.setSize(rows, cols);

                if (rows!=matrix.N() or cols!=matrix.M()) {
                    DUNE_THROW(Exception, "The matrix provided by the specified file has wrong dimensions!");
                } else {

                    typedef typename MatrixType::row_type RowType;
                    typedef typename RowType::ConstIterator ColumnIterator;

                    for(size_t rowIdx = 0; rowIdx<rows; rowIdx++) {
                        const RowType& row = matrix[rowIdx];

                        ColumnIterator cIt    = row.begin();
                        ColumnIterator cEndIt = row.end();

                        getline(file, line);
                        lineSplit.clear();
                        split(line, ' ', lineSplit);

                        for(; cIt!=cEndIt; ++cIt) {
                            matrix[rowIdx][cIt.index()] = atof(lineSplit[cIt.index()].c_str());
                        }
                    }
                }

                file.close();
            } else {
                DUNE_THROW(Exception, "Could not open " << filePath_ << " for reading!");
            }
        }



};
#endif
