#ifndef GLOBAL_FAULT_ASSEMBLER_HH
#define GLOBAL_FAULT_ASSEMBLER_HH

#include <dune/fufem/assemblers/assembler.hh>
#include "dune/fufem/assemblers/boundaryfunctionalassembler.hh"
#include "dune/fufem/boundarypatch.hh"

#include <dune/istl/matrixindexset.hh>

#include <dune/faultnetworks/assemblers/interfaceoperatorassembler.hh>
#include <dune/faultnetworks/levelinterfacenetwork.hh>
#include <dune/faultnetworks/faultinterface.hh>

#include <dune/faultnetworks/utils/debugutils.hh>

//! Generic global assembler for operators and functionals
template <class TrialBasis, class AnsatzBasis>
class GlobalFaultAssembler
{

    private:
    typedef typename TrialBasis::GridView::Grid GridType;
    typedef typename TrialBasis::GridView GridView;
    typedef typename GridType::LevelIntersection Intersection;

    const TrialBasis& tBasis_;
    const AnsatzBasis& aBasis_;

    const LevelInterfaceNetwork<GridView>& levelInterfaceNetwork_;

    template <class MatrixType>
    void add(MatrixType& A, MatrixType& B, MatrixType& res) const {
        if (A.N()!=B.N() || A.M()!=B.M())
            DUNE_THROW(Dune::RangeError, "GlobalProblemAssembler::add() Matrices must have same dimensions!");

        typedef typename std::map<std::pair<size_t, size_t>, typename MatrixType::field_type> MatEntriesType;
        typedef typename MatEntriesType::iterator MatEntriesIterator;
        typedef typename MatrixType::row_type RowType;
        typedef typename RowType::ConstIterator ColumnIterator;

        MatEntriesType matEntries;
        MatEntriesIterator entryIt;

        Dune::MatrixIndexSet joinedIdxSet(A.N(), A.M());
        joinedIdxSet.import(A);

        for(size_t rowIdx = 0; rowIdx<B.N(); rowIdx++) {
            const RowType& BRow = B[rowIdx];
            const RowType& ARow = A[rowIdx];

            ColumnIterator cIt    = BRow.begin();
            ColumnIterator cEndIt = BRow.end();

            for(; cIt!=cEndIt; ++cIt) {
                const size_t colIdx = cIt.index();

                if (A.exists(rowIdx, colIdx)) {
                    matEntries[std::make_pair(rowIdx, colIdx)] = ARow[colIdx] + BRow[colIdx];
                } else {
                    matEntries[std::make_pair(rowIdx, colIdx)] = BRow[colIdx];
                    joinedIdxSet.add(rowIdx, colIdx);
                }
            }
        }

        joinedIdxSet.exportIdx(res);

        for(size_t rowIdx = 0; rowIdx<res.N(); rowIdx++) {
            RowType& row = res[rowIdx];
            const RowType& ARow = A[rowIdx];

            ColumnIterator cIt    = row.begin();
            ColumnIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt) {
                const size_t colIdx = cIt.index();
                entryIt = matEntries.find(std::make_pair(rowIdx, colIdx));

                if (entryIt==matEntries.end()) {
                    row[colIdx] = ARow[colIdx];
                } else {
                    row[colIdx] = entryIt->second;
                }
            }
        }
    }

    public:
        //! create assembler
        GlobalFaultAssembler(const TrialBasis& tBasis, const AnsatzBasis& aBasis, const LevelInterfaceNetwork<GridView>& levelInterfaceNetwork) :
            tBasis_(tBasis),
            aBasis_(aBasis),
            levelInterfaceNetwork_(levelInterfaceNetwork)
        {}

        template <class LocalAssemblerType, class LocalInterfaceAssembler, class MatrixType>
        void assembleOperator(LocalAssemblerType& localAssembler, const std::vector<std::shared_ptr<LocalInterfaceAssembler>>& localInterfaceAssemblers, MatrixType& A, const bool lumping=false) const
        {
            assert(localInterfaceAssemblers.size() == levelInterfaceNetwork_.size());

            // assemble stiffness matrix
            MatrixType stiffMat;
            Assembler<TrialBasis, AnsatzBasis> assembler(tBasis_, aBasis_);
            assembler.assembleOperator(localAssembler, stiffMat, lumping);

            MatrixType interfaceMat;
            const std::vector<Intersection>& intersections = levelInterfaceNetwork_.getIntersections();

            InterfaceOperatorAssembler<TrialBasis, AnsatzBasis> interfaceAssembler(tBasis_, aBasis_, intersections);
            interfaceAssembler.assemble(localInterfaceAssemblers, interfaceMat, lumping);

            add(stiffMat, interfaceMat, A);
        }

        template <class LocalFunctionalAssemblerType, class VectorType>
        void assembleFunctional(LocalFunctionalAssemblerType& localAssembler, VectorType& b, bool initializeVector=true) const
        {
            Assembler<TrialBasis, AnsatzBasis> assembler(tBasis_, aBasis_);
            assembler.assembleFunctional(localAssembler, b, initializeVector);
        }


        template <class LocalBoundaryFunctionalAssemblerType, class VectorType>
        void assembleBoundaryFunctional(LocalBoundaryFunctionalAssemblerType& localAssembler,
                                        VectorType& b,
                                        const BoundaryPatch<typename TrialBasis::GridView>& boundaryPatch,
                                        bool initializeVector=true) const
        {
            const BoundaryFunctionalAssembler<TrialBasis> boundaryFunctionalAssembler(tBasis_, boundaryPatch);
            boundaryFunctionalAssembler.assemble(localAssembler, b, initializeVector);
        }
};

#endif

