#ifndef FAULTNETWORKS_VECTOR_READER_HH
#define FAULTNETWORKS_VECTOR_READER_HH

#include <fstream>

// dune-common

#include <dune/common/exceptions.hh>





using namespace Dune;


template <class VectorType>
class VectorReader {
    private: 
        std::string filePath_;

        void split(const std::string& str, char delimiter, std::vector<std::string>& elements) {
            std::stringstream strStream(str);
            std::string token;
            
            while (std::getline(strStream, token, delimiter)) {
                elements.push_back(token);
            }
        }

     
    public: 
        typedef VectorType vector_type;

        void setSource(const std::string filePath) {
            filePath_ = filePath;
        }


        VectorReader(const std::string filePath) {
            filePath_ = filePath;
        }

        
        void read(VectorType& vector) {
            std::string line;
            std::ifstream file (filePath_);

            if (file.is_open()) {

                getline (file,line);

                size_t size = atoi(line.c_str());
                vector.resize(size);

                for(size_t it = 0; it<size; it++) {
                    getline(file, line);

                    vector[it] = atof(line.c_str());
                }

                file.close();
            } else {
                DUNE_THROW(Exception, "Could not open " << filePath_ << " for reading!");
            }
        }

        void write(VectorType& vector) {
            std::ofstream file;
            file.open(filePath_);

            if (file.is_open()) {
                std::ostringstream sizeStr;
                sizeStr << vector.size();
                file << sizeStr.str() << std::endl;

                for(size_t it = 0; it<vector.size(); it++) {
                    std::ostringstream strs;
                    strs << vector[it];
                    file << strs.str() << std::endl;
                }

                file.close();
            } else {
                DUNE_THROW(Exception, "Could not open " << filePath_ << " for writing!");
            }
        }

};
#endif
