#ifndef OSC_LOCAL_ASSEMBLER_HH
#define OSC_LOCAL_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

//#include <dune/istl/matrix.hh>

#include "dune/fufem/staticmatrixtools.hh"
#include "dune/fufem/quadraturerules/quadraturerulecache.hh"
#include "dune/fufem/assemblers/localoperatorassembler.hh"

#include <dune/grid/common/mcmgmapper.hh>


/** \brief Local assembler */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1> > 
class OscLocalAssembler : public LocalOperatorAssembler <GridType, TrialLocalFE, AnsatzLocalFE, T >
{
    private:
        typedef LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE ,T > Base;
        typedef typename GridType::ctype ctype;
        static const int dim      = GridType::dimension;
        static const int dimworld = GridType::dimensionworld;
        typedef typename Dune::FieldVector<ctype, dimworld> VectorType;

    public:
        typedef typename Base::Element Element;
        typedef typename Element::Geometry Geometry;
        typedef typename Geometry::JacobianInverseTransposed JacobianInverseTransposed;
        typedef typename Base::BoolMatrix BoolMatrix;
        typedef typename Base::LocalMatrix LocalMatrix;
        typedef typename Dune::LevelMultipleCodimMultipleGeomTypeMapper <GridType,  Dune::MCMGElementLayout > MapperType; 

    private:
        const int quadOrder_;

        const std::vector<ctype>& oscData_;
        const MapperType& mapper_;


    public:
        OscLocalAssembler(const std::vector<ctype>& oscData, const MapperType& mapper) :
            quadOrder_(-1), oscData_(oscData), mapper_(mapper)
        {}

        DUNE_DEPRECATED_MSG("Quadrature order is now selected automatically. you don't need to specify it anymore.")
        OscLocalAssembler(int quadOrder, const std::vector<ctype>& oscData, const MapperType& mapper) :
            quadOrder_(quadOrder), oscData_(oscData), mapper_(mapper)
        {}

        void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
        }

        void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename Dune::template FieldVector<double,dim> FVdim;
            typedef typename Dune::template FieldVector<double,dimworld> FVdimworld;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;
            
            // Make sure we got suitable shape functions
            assert(tFE.type() == element.type());
            assert(aFE.type() == element.type());

            // check if ansatz local fe = test local fe
            if (not Base::isSameFE(tFE, aFE))
                DUNE_THROW(Dune::NotImplemented, "LaplaceAssembler is only implemented for ansatz space=test space!");

            int rows = localMatrix.N();
            int cols = localMatrix.M();

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localMatrix = 0.0;

            // get quadrature rule
            QuadratureRuleKey tFEquad(tFE);
            QuadratureRuleKey quadKey = tFEquad.derivative().square();
            if (quadOrder_>=0)
                quadKey.setOrder(quadOrder_);
            const Dune::template QuadratureRule<double, dim>& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

            // store gradients of shape functions and base functions
            std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
            std::vector<FVdimworld> gradients(tFE.localBasis().size());

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const FVdim& quadPos = quad[pt].position();

                // get transposed inverse of Jacobian of transformation
                const JacobianInverseTransposed& invJacobian = geometry.jacobianInverseTransposed(quadPos);

                // get integration factor
                const double integrationElement = geometry.integrationElement(quadPos) * oscData_[mapper_.index(element)];

                // get gradients of shape functions
                tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

                // transform gradients
                for (size_t i=0; i<gradients.size(); ++i)
                    invJacobian.mv(referenceGradients[i][0], gradients[i]);

                // compute matrix entries
                double z = quad[pt].weight() * integrationElement;
                for (int i=0; i<rows; ++i)
                {
                    for (int j=i+1; j<cols; ++j)
                    {
                        double zij = (gradients[i] * gradients[j]) * z;
                        StaticMatrix::addToDiagonal(localMatrix[i][j],zij);
                        StaticMatrix::addToDiagonal(localMatrix[j][i],zij);
                    }
                    StaticMatrix::addToDiagonal(localMatrix[i][i], (gradients[i] * gradients[i]) * z);
                }
            }
            return;
        }
};


#endif

