#ifndef MULTILEVEL_PATCH_PRECONDITIONER_HH
#define MULTILEVEL_PATCH_PRECONDITIONER_HH

#include <string>

#include <dune/common/timer.hh>
#include <dune/common/fvector.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/solvers/iterationsteps/lineariterationstep.hh>

#include <dune/faultnetworks/preconditioners/levelglobalpreconditioner.hh>
#include <dune/faultnetworks/preconditioners/levelpatchpreconditioner.hh>
#include <dune/faultnetworks/preconditioners/cellpatchpreconditioner.hh>
#include <dune/faultnetworks/preconditioners/supportpatchpreconditioner.hh>
#include <dune/faultnetworks/levelinterfacenetwork.hh>
#include <dune/faultnetworks/interfacenetwork.hh>
#include <dune/faultnetworks/dgmgtransfer.hh>

#include <dune/faultnetworks/utils/debugutils.hh>

template <class BasisType, class LocalOperatorAssembler, class LocalInterfaceAssembler, class MatrixType, class VectorType>
class MultilevelPatchPreconditioner : public LinearIterationStep<MatrixType, VectorType> {

public:
    using LevelPatchPreconditionerType = LevelPatchPreconditioner<BasisType, LocalOperatorAssembler, LocalInterfaceAssembler, MatrixType, VectorType>;
    using LevelGlobalPreconditionerType = LevelGlobalPreconditioner<BasisType, LocalOperatorAssembler, LocalInterfaceAssembler, MatrixType, VectorType>;

private:
    using GridView = typename BasisType::GridView;
    using GridType = typename GridView::Grid ;
    using BitVector = typename Dune::BitSetVector<1>;

    const InterfaceNetwork<GridType>& interfaceNetwork_;
    const BitVector& activeLevels_;
    const std::vector<std::shared_ptr<LocalOperatorAssembler>>& localAssemblers_;
    const std::vector<std::vector<std::shared_ptr<LocalInterfaceAssembler>>>& localInterfaceAssemblers_;

    // preconditioner parset settings
    const Dune::ParameterTree& parset_;
    using Mode = typename LevelPatchPreconditionerType::Mode;
    Mode mode_;

    using MultDirection = typename LevelPatchPreconditionerType::Direction;
    MultDirection multDirection_;


    int itCounter_;

    std::shared_ptr<LevelGlobalPreconditionerType> coarseGlobalPreconditioner_;
    std::vector<std::shared_ptr<LevelPatchPreconditionerType>> levelPatchPreconditioners_;
    std::vector<VectorType> levelX_;
    std::vector<VectorType> levelRhs_;

    std::shared_ptr<LevelInterfaceNetwork<GridView>> allFaultLevelInterfaceNetwork_;
    std::shared_ptr<BasisType> allFaultBasis_;

    std::vector<std::shared_ptr<DGMGTransfer<BasisType>>> mgTransfer_;

public:
    MultilevelPatchPreconditioner(const InterfaceNetwork<GridType>& interfaceNetwork,
                                  const BitVector& activeLevels,
                                  const std::vector<std::shared_ptr<LocalOperatorAssembler>>& localAssemblers,
                                  const std::vector<std::vector<std::shared_ptr<LocalInterfaceAssembler>>>& localInterfaceAssemblers,
                                  const Dune::ParameterTree& parset) :
          interfaceNetwork_(interfaceNetwork),
          activeLevels_(activeLevels),
          localAssemblers_(localAssemblers),
          localInterfaceAssemblers_(localInterfaceAssemblers),
          parset_(parset)
    {
        parseSettings();

        if (activeLevels_.size() > (size_t) interfaceNetwork.maxLevel() +1)
            DUNE_THROW(Dune::Exception, "MultilevelFaultPreconditioner: too many active levels; preconditioner supports at most (grid.maxLevel + 1) levels!");

        assert(activeLevels.size() == localAssemblers_.size() && activeLevels.size() == localInterfaceAssemblers_.size());

        // init level fault preconditioners and multigrid transfer
        levelPatchPreconditioners_.resize(0);
        mgTransfer_.resize(0);

        for (size_t i=0; i<activeLevels_.size(); i++) {
            if (activeLevels_[i][0]) {
                // init global problem on coarsest level
                const LevelInterfaceNetwork<GridView>& levelNetwork = interfaceNetwork.levelInterfaceNetwork(i);
                coarseGlobalPreconditioner_ = std::make_shared<LevelGlobalPreconditionerType>(levelNetwork, *localAssemblers_[i], localInterfaceAssemblers_[i]);

                break;
            }
        }

        auto maxLevel = setupLevelPreconditioners();

        levelX_.resize(levelPatchPreconditioners_.size()+1);
        levelRhs_.resize(levelPatchPreconditioners_.size()+1);

        allFaultBasis_ = std::make_shared<BasisType>(interfaceNetwork_.levelInterfaceNetwork(maxLevel));

        // init multigrid transfer
        for (size_t i=0; i<levelPatchPreconditioners_.size(); i++) {
            mgTransfer_.push_back(std::make_shared<DGMGTransfer<BasisType>>(levelPatchPreconditioners_[i]->basis(), *allFaultBasis_));
        }
        mgTransfer_.push_back(std::make_shared<DGMGTransfer<BasisType>>(coarseGlobalPreconditioner_->basis(), *allFaultBasis_));

        itCounter_ = 0;
    }


   virtual void setProblem(const MatrixType& mat, VectorType& x, const VectorType& rhs) {
        this->x_ = &x;
        this->rhs_ = &rhs;
        this->mat_ = Dune::stackobject_to_shared_ptr(mat);

        for (size_t i=0; i<levelPatchPreconditioners_.size(); i++) {
            mgTransfer_[i]->restrict(x, levelX_[i]);
            mgTransfer_[i]->restrict(rhs, levelRhs_[i]);

            levelPatchPreconditioners_[i]->setProblem(mat, levelX_[i], levelRhs_[i]);
        }

        size_t j = levelPatchPreconditioners_.size();
        mgTransfer_[j]->restrict(x, levelX_[j]);
        mgTransfer_[j]->restrict(rhs, levelRhs_[j]);

        coarseGlobalPreconditioner_->setProblem(mat, levelX_[j], levelRhs_[j]);
    }

    virtual void iterate() {
        if (mode_ == Mode::ADDITIVE)
            iterateAdd();
        else
            iterateMult();
    }

    void build() {
        coarseGlobalPreconditioner_->build();

        for (size_t i=0; i<levelPatchPreconditioners_.size(); i++) {
            levelPatchPreconditioners_[i]->build();
        }
    }

    std::shared_ptr<LevelPatchPreconditionerType> levelPatchPreconditioner(const int level) {
        return levelPatchPreconditioners_[level];
    }

    const BasisType& basis() const {
        return *allFaultBasis_;
    }

    size_t size() const {
        return levelPatchPreconditioners_.size()+1;
    }

private:
    void parseSettings() {
        const auto mode = parset_.get<std::string>("mode");
        if (mode == "ADDITIVE") {
            mode_ = Mode::ADDITIVE;
        } else if (mode == "MULTIPLICATIVE") {
            mode_ = Mode::MULTIPLICATIVE;
        } else {
            DUNE_THROW(Dune::Exception, "MultilevelFaultPreconditioner::parseSettings unknown mode! Possible options: ADDITIVE , MULTIPLICATIVE");
        }

        auto multDirection = parset_.get<std::string>("multDirection");
        if (multDirection == "FORWARD") {
            multDirection_ = MultDirection::FORWARD;
        } else if (multDirection == "BACKWARD") {
            multDirection_ = MultDirection::BACKWARD;
        } else if (multDirection == "SYMMETRIC") {
            multDirection_ = MultDirection::SYMMETRIC;
        } else {
            DUNE_THROW(Dune::Exception, "MultilevelFaultPreconditioner::parseSettings unknown multDirection! Possible options: FORWARD , BACKWARD, SYMMETRIC");
        }
    }

    void iterateAdd() {
        VectorType x;

        for (size_t i=0; i<levelPatchPreconditioners_.size(); i++) {
            levelPatchPreconditioners_[i]->iterate();
            const VectorType& it = levelPatchPreconditioners_[i]->getSol();

            mgTransfer_[i]->prolong(it, x);

            //writeToVTK(*allFaultBasis_, x, "/home/mi/podlesny/data/faultnetworks/sol/", "preconditioner_step_"+std::to_string(i));

            *(this->x_) += x;
        }

        coarseGlobalPreconditioner_->iterate();
        const VectorType& it = coarseGlobalPreconditioner_->getSol();

        mgTransfer_[levelPatchPreconditioners_.size()]->prolong(it, x);
        *(this->x_) += x;
    }


    void iterateMult() {

        if (multDirection_ != MultDirection::FORWARD)
            for (size_t i=levelPatchPreconditioners_.size()-1; i>=0 && i<levelPatchPreconditioners_.size(); i--)
                iterateStep(i, MultDirection::BACKWARD);

        size_t j = levelPatchPreconditioners_.size();
        mgTransfer_[j]->restrict(*(this->x_), levelX_[j]);

        //print(levelRhs_[j], "localCoarseRhs: ");
        //writeToVTK(coarseGlobalPreconditioner_->basis(), levelRhs_[j], "/storage/mi/podlesny/data/faultnetworks/rhs/coarse", "exactvertexdata_step_"+std::to_string(itCounter_));

        VectorType residual = *(this->rhs_);
        Dune::MatrixVector::subtractProduct(residual, *(this->mat_), *(this->x_));

        VectorType localResidual;
        mgTransfer_[j]->restrict(residual, localResidual);

        coarseGlobalPreconditioner_->setProblem(*(this->mat_), levelX_[j], localResidual);
        coarseGlobalPreconditioner_->iterate();
        const VectorType& it = coarseGlobalPreconditioner_->getSol();

        VectorType x;
        mgTransfer_[j]->prolong(it, x);
        *(this->x_) += x;
        //writeToVTK(*allFaultBasis_, x, "/storage/mi/podlesny/data/faultnetworks/coarseIterates/multilevel", "exactvertexdata_step_"+std::to_string(itCounter_));

        if (multDirection_ != MultDirection::BACKWARD) {
            for (size_t i=0; i<levelPatchPreconditioners_.size(); i++)
                iterateStep(i, MultDirection::FORWARD);
        }

        itCounter_++;
    }

    void iterateStep(const size_t i, const MultDirection dir) {
        //mgTransfer_[i]->restrict(*(this->x_), levelX_[i]);

        VectorType residual = *(this->rhs_);
        Dune::MatrixVector::subtractProduct(residual, *(this->mat_), *(this->x_));

        VectorType localResidual;
        mgTransfer_[i]->restrict(residual, localResidual);
        //print(levelRhs_[i], "levelLocalCoarseRhs: ");
        //writeToVTK(levelPatchPreconditioners_[i]->basis(), levelRhs_[i], "/storage/mi/podlesny/data/faultnetworks/rhs/fine", "exactvertexdata_step_"+std::to_string(itCounter_));


        levelPatchPreconditioners_[i]->setProblem(*(this->mat_), levelX_[i], localResidual);

        levelPatchPreconditioners_[i]->setDirection(dir);
        levelPatchPreconditioners_[i]->iterate();
        const VectorType& it = levelPatchPreconditioners_[i]->getSol();

        VectorType x;
        mgTransfer_[i]->prolong(it, x);
        *(this->x_) += x;

        //writeToVTK(*allFaultBasis_, x, "/storage/mi/podlesny/data/faultnetworks/fineIterates/multilevel", "exactvertexdata_step_"+std::to_string(itCounter_));
    }

    auto setupLevelPreconditioners() {
        const auto preconditionerType = parset_.get<std::string>("patch");
        if (preconditionerType == "CELL") {
            return setupCellPreconditioner();
        } else if (preconditionerType == "SUPPORT") {
            return setupSupportPreconditioner();
        } else {
            DUNE_THROW(Dune::Exception, "MultilevelFaultPreconditioner: unknown levelpatchpreconditioner type! Possible options: CELL , SUPPORT");
        }
    }

    auto setupCellPreconditioner() {
        using CellPreconditioner = CellPatchPreconditioner<BasisType, LocalOperatorAssembler, LocalInterfaceAssembler, MatrixType, VectorType>;

        int maxLevel = 0;

        bool skip = true;
        for (size_t i=0; i<activeLevels_.size(); i++) {
            if (activeLevels_[i][0] && skip) {
                skip = false;
                continue;
            }

            if (activeLevels_[i][0]) {
                const LevelInterfaceNetwork<GridView>& levelNetwork = interfaceNetwork_.levelInterfaceNetwork(i);

                if (levelPatchPreconditioners_.size() == 0) {
                    levelPatchPreconditioners_.push_back(std::make_shared<CellPreconditioner>(levelNetwork, coarseGlobalPreconditioner_->basis(), *localAssemblers_[i], localInterfaceAssemblers_[i], mode_));
                } else {
                    levelPatchPreconditioners_.push_back(std::make_shared<CellPreconditioner>(levelNetwork, levelPatchPreconditioners_.back()->basis(), *localAssemblers_[i], localInterfaceAssemblers_[i], mode_));
                    levelPatchPreconditioners_.back()->setBoundaryMode(LevelPatchPreconditionerType::BoundaryMode::homogeneous);
                }

                maxLevel = i;
            }
        }

        return maxLevel;
    }

    auto setupSupportPreconditioner() {
        using SupportPreconditioner = SupportPatchPreconditioner<BasisType, LocalOperatorAssembler, LocalInterfaceAssembler, MatrixType, VectorType>;

        const auto patchDepth = parset_.get<size_t>("patchDepth");

        int maxLevel = 0;

        bool skip = true;
        for (size_t i=0; i<activeLevels_.size(); i++) {
            if (skip && activeLevels_[i][0]) {
                skip = false;
                continue;
            }

            if (activeLevels_[i][0]) {
                // init local patch preconditioners on each level
                const LevelInterfaceNetwork<GridView>& levelNetwork = interfaceNetwork_.levelInterfaceNetwork(i);

                if (levelPatchPreconditioners_.size() == 0) {
                    levelPatchPreconditioners_.push_back(std::make_shared<SupportPreconditioner>(levelNetwork, coarseGlobalPreconditioner_->basis(), *localAssemblers_[i], localInterfaceAssemblers_[i], mode_));
                } else {
                    levelPatchPreconditioners_.push_back(std::make_shared<SupportPreconditioner>(levelNetwork, levelPatchPreconditioners_.back()->basis(), *localAssemblers_[i], localInterfaceAssemblers_[i], mode_));
                    levelPatchPreconditioners_.back()->setPatchDepth(patchDepth);
                    levelPatchPreconditioners_.back()->setBoundaryMode(LevelPatchPreconditionerType::BoundaryMode::homogeneous);
                }

                maxLevel = i;
            }
        }

        return maxLevel;
    }
};

#endif

