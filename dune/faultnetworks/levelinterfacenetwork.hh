#ifndef LEVEL_INTERFACE_NETWORK_HH
#define LEVEL_INTERFACE_NETWORK_HH


#include <dune/common/bitsetvector.hh>
#include <dune/common/fvector.hh>

#include <dune/faultnetworks/faultinterface.hh>

template <typename GridView>
class LevelInterfaceNetwork
{
    public:
        typedef typename GridView::Grid GridType;
        typedef typename GridType::ctype ctype;
        static const int dim = GridType::dimension;
        typedef typename GridType::LevelIntersection Intersection;

    protected:	
        typedef typename GridType::template Codim<0>::Entity Element;
        static const int dimElement = Element::dimension;
    private:
        GridView gridView_;
        const int level_;

        std::vector<Intersection> intersections_;
        std::vector<int> intersectionsLevels_;
        std::map<std::pair<size_t, size_t>, bool> isInterfaceIntersectionMapper_;

        std::set<size_t> interfaceNetworkDofs_;

        typedef typename Dune::LevelMultipleCodimMultipleGeomTypeMapper <GridType,  Dune::MCMGElementLayout > ElementMapper;
        ElementMapper elementMapper_;

        void computeIntersectionDofs(const Intersection& intersection, std::set<size_t>& intersectionDofs) {
            intersectionDofs.clear();

            // loop over all vertices of the intersection
            const Element& insideElement = intersection.inside();

            const auto& refElement = Dune::ReferenceElements<double,dimElement>::general(insideElement.type());
            for (int i=0; i<refElement.size(intersection.indexInInside(), 1, dimElement); i++) {
                size_t idxInElement = refElement.subEntity(intersection.indexInInside(), 1, i, dimElement);
                size_t globalIdx = gridView_.indexSet().subIndex(insideElement, idxInElement, dimElement);
                intersectionDofs.insert(globalIdx);
            }
        }

	public:
        LevelInterfaceNetwork(const GridType& grid, const int level) :
            gridView_(grid.levelGridView(level)), level_(level), elementMapper_(grid, level_) {
            intersections_.resize(0);
            intersectionsLevels_.resize(0);
            isInterfaceIntersectionMapper_.clear();
            interfaceNetworkDofs_.clear();
        }


        size_t size() const {
            return intersections_.size();
        }

        /*
    const std::shared_ptr<FaultInterface<GridView>> getInterface(const size_t interfaceID) const {
        return interfaces_[interfaceID];
    }*/

    bool isInterfaceIntersection(const Intersection& intersection) const {
        size_t elementIdx = elementMapper_.index(intersection.inside());
        return isInterfaceIntersectionMapper_.count(std::make_pair(elementIdx, intersection.indexInInside()));
    }

    bool isInterfaceIntersection(size_t elementIdx, size_t intersectionIdx) const {
        return isInterfaceIntersectionMapper_.count(std::make_pair(elementIdx, intersectionIdx));
    }

    // "inside" direction might change depending on intersection
    /*bool inInside(const Element& elem, const size_t globalIdx) const {
          typename std::map<size_t, std::shared_ptr<FaultInterface<GridView>>>::const_iterator it = dofToInterface_.find(globalIdx);

          if (it == dofToInterface_.end())
              return false;
          else
              return (it->second)->inInside(elem);
    }*/

    void addIntersection(const Intersection& intersection, const int intersectionLevel) {
        size_t insideElementIdx = elementMapper_.index(intersection.inside());
        bool isNewIntersection = !isInterfaceIntersection(insideElementIdx, intersection.indexInInside());

        if (intersection.neighbor()) {
            size_t outsideElementIdx = elementMapper_.index(intersection.outside());
            isNewIntersection = isNewIntersection && !isInterfaceIntersection(outsideElementIdx, intersection.indexInOutside());
        }

        //std::cout << "isNewIntersection: " << isNewIntersection << std::endl;

        if (isNewIntersection) {
            intersections_.push_back(intersection);
            intersectionsLevels_.push_back(intersectionLevel);

            // add to interfaceDofs_
            std::set<size_t> intersectionDofs;
            computeIntersectionDofs(intersection, intersectionDofs);

            interfaceNetworkDofs_.insert(intersectionDofs.begin(), intersectionDofs.end());

            // add to isInterfaceIntersectionMapper_
            isInterfaceIntersectionMapper_[std::make_pair(insideElementIdx, intersection.indexInInside())] = true;

            if (intersection.neighbor()) {
                size_t outsideElementIdx = elementMapper_.index(intersection.outside());
                isInterfaceIntersectionMapper_[std::make_pair(outsideElementIdx, intersection.indexInOutside())] = true;
            }
        }
    }

    void addIntersection(const Intersection& intersection) {
        addIntersection(intersection, level_);
    }

    void addInterface(std::shared_ptr<FaultInterface<GridView>> interface){
        const std::vector<Intersection>& intersections = interface->faces();
        const int fatherLevel = interface->coarsestFather()->level();

        for (size_t i=0; i<intersections.size(); i++) {
            addIntersection(intersections[i], fatherLevel);
        }
    }
    
	
	// running local index of interface dof
    size_t localIndex(const size_t i) const {
        std::set<size_t>::iterator it;
			
		it = interfaceNetworkDofs_.find(i);

		if (it == interfaceNetworkDofs_.end())
			DUNE_THROW(Dune::Exception, "Global index of dof not part of the interface network.");

		return std::distance(interfaceNetworkDofs_.begin(), it);
	}
	
    const std::vector<Intersection>& getIntersections() const {
        return intersections_;
    }

    const std::vector<int>& getIntersectionsLevels() const {
        return intersectionsLevels_;
    }

    size_t dofCount() const {
		return interfaceNetworkDofs_.size(); 
    }

    const std::set<size_t>& getInterfaceNetworkDofs() const{
        return interfaceNetworkDofs_;
    }

    int level() const {
        return level_;
    }

    const GridView& levelGridView() const {
        return gridView_;
    }

    const GridType& grid() const {
        return gridView_.grid();
    }
};

#endif
